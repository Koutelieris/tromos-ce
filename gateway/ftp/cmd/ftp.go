// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
	"os/exec"
)

func init() {
	gatewayCmd.AddCommand(ftpCmd)

	ftpCmd.AddCommand(ftpStartCmd)
	ftpStartCmd.Flags().String("config", "", "FTPd configuration file")
	ftpStartCmd.MarkFlagRequired("config")

	ftpCmd.AddCommand(ftpStopCmd)
}

var ftpCmd = &cobra.Command{
	Use:   "ftp",
	Short: "Handle ftp",
	Long:  `blah blah blah fuse`,
}

var ftpStartCmd = &cobra.Command{
	Use:   "start",
	Short: "start ftp server",
	Long:  `blah blah blah ftp`,
	Run: func(cmd *cobra.Command, args []string) {

		config, err := cmd.Flags().GetString("config")
		if err != nil {
			log.Fatal(err)
		}

		c := exec.Command("./bin/gateway/gitlab.com/tromos/tromos-ce/engineftp", config)
		c.Stdout = os.Stdout
		c.Stderr = os.Stderr
		if err := c.Run(); err != nil {
			log.Fatal(err)
		}
		//profile()
	},
}

var ftpStopCmd = &cobra.Command{
	Use:   "stop",
	Short: "Terminate the ftp server",
	Long:  `Long explanation message`,
	Run: func(cmd *cobra.Command, args []string) {

		c := exec.Command("pkill gitlab.com/tromos/tromos-ce/engineftp")
		c.Stdout = os.Stdout
		c.Stderr = os.Stderr
		err := c.Run()
		if err != nil {
			log.Fatal(err)
		}
		//profile()
	},
}
