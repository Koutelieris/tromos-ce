package driver

import (
	"bytes"
	"github.com/goftp/server"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/logical/augmented"
	"io"
	"io/ioutil"
	"os"
	"time"
)

const Perm = 0777 // Read + execute (for cd)

type FileInfo struct {
	name     string
	size     int64
	mode     os.FileMode
	modetime time.Time
	owner    string
	group    string
}

func (f *FileInfo) Name() string {
	return f.name
}

func (f *FileInfo) Size() int64 {
	return f.size
}

func (f *FileInfo) Mode() os.FileMode {
	return f.mode
}

func (f *FileInfo) ModTime() time.Time {
	return f.modetime
}

func (f *FileInfo) IsDir() bool {
	return false
}

func (f *FileInfo) Sys() interface{} {
	return nil
}

func (f *FileInfo) Owner() string {
	return "gitlab.com/tromos/tromos-ce/engine"
}

func (f *FileInfo) Group() string {
	return "gitlab.com/tromos/tromos-ce/engine"
}

// Follow the singleton pattern
type TromosDriverFactory struct {
	Manifest  string
	Blocksize int
	storage   *middleware.Client
}

func (factory TromosDriverFactory) Init() TromosDriverFactory {
	log.Admin("Build storage")

	conf := middleware.ClientConfig{Manifest: factory.Manifest}

	client, err := middleware.NewClient(conf)
	if err != nil {
		panic(err)
	}
	factory.storage = client

	return factory
}

func (factory TromosDriverFactory) NewDriver() (server.Driver, error) {
	log.Admin("Start new Tromos driver")

	driver := TromosDriver{storage: factory.storage, blocksize: factory.Blocksize}
	return &driver, nil
}

type TromosDriver struct {
	storage   *middleware.Client
	blocksize int
}

// Init
func (driver *TromosDriver) Init(*server.Conn) {}

// params  - a file path
// returns - a time indicating when the requested path was last modified
//         - an error if the file doesn't exist or the user lacks
//           permissions
func (driver *TromosDriver) Stat(name string) (server.FileInfo, error) {
	rtx, err := driver.storage.BeginTx(name, middleware.VIEW)
	if err != nil {
		rtx.Rollback()
		return nil, err
	}

	tree := augmented.NewTree(0, 0)
	rtx.Cursor().ForEach(func(stx *middleware.SubTx) bool {
		tree.Add(int64(stx.Offset), int64(stx.Size))
		return true
	})

	info := &FileInfo{
		name: name,
		size: tree.Length(),
	}
	return info, nil
}

// params  - path
// returns - true if the current user is permitted to change to the
//           requested path
func (driver *TromosDriver) ChangeDir(string) error {
	panic("Not implemented yet")
}

// params  - path, function on file or subdir found
// returns - error
//           path
func (driver *TromosDriver) ListDir(string, func(server.FileInfo) error) error {
	panic("Not implemented yet")
}

// params  - path
// returns - nil if the directory was deleted or any error encountered
func (driver *TromosDriver) DeleteDir(string) error {
	panic("Not implemented yet")
}

// params  - path
// returns - nil if the file was deleted or any error encountered
func (driver *TromosDriver) DeleteFile(name string) error {
	return driver.storage.Remove(name)
}

// params  - from_path, to_path
// returns - nil if the file was renamed or any error encountered
func (driver *TromosDriver) Rename(string, string) error {
	panic("Not implemented yet")
}

// params  - path
// returns - nil if the new directory was created or any error encountered
func (driver *TromosDriver) MakeDir(string) error {
	panic("Not implemented yet")
}

// params  - path
// returns - a string containing the file data to send to the client
func (driver *TromosDriver) GetFile(name string, offset int64) (int64, io.ReadCloser, error) {

	log.Admin("Get file:", name, " offset:", offset)

	tx, err := driver.storage.BeginTx(name, middleware.RDONLY)
	if err != nil {
		tx.Rollback()
		return 0, nil, err
	}
	defer tx.Rollback()

	cache := []byte{}
	return int64(len(cache)) - offset, ioutil.NopCloser(bytes.NewReader(cache)), nil
}

// params  - destination path, an io.Reader containing the file data
// returns - the number of bytes writen and the first error encountered while writing, if any.
func (driver *TromosDriver) PutFile(name string, reader io.Reader, append bool) (int64, error) {

	if err := driver.storage.CreateOrReset(name); err != nil {
		return 0, err
	}

	tx, err := driver.storage.BeginTx(name, middleware.WONLY)
	if err != nil {
		return 0, err
	}

	wb, err := tx.Update(0, func(pw io.ReadWriteCloser) (int, error) {
		wb, err := io.Copy(pw, reader)
		return int(wb), err
	})

	if err != nil {
		tx.Rollback()
		return 0, err
	}

	if err := tx.Commit(); err != nil {
		return int64(wb), err
	}

	return int64(wb), nil
}
