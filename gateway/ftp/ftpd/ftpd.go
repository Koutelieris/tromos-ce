package main

import (
	"fmt"
	"github.com/Unknwon/goconfig"
	"github.com/goftp/file-driver"
	"github.com/goftp/ftpd/web"
	"github.com/goftp/server"
	"gitlab.com/tromos/tromos-ce/gateway/ftp/driver"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"os"
)

func main() {

	cfg, err := goconfig.LoadConfigFile(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	port, _ := cfg.Int("server", "port")
	//auth := server.SimpleAuth{"paparia", "mentoles"}
	perm := server.NewSimplePerm("root", "root")

	typ, _ := cfg.GetValue("driver", "type")
	var factory server.DriverFactory
	if typ == "file" {
		log.Admin("Start with file driver")
		rootPath, _ := cfg.GetValue("file", "rootpath")
		_, err = os.Lstat(rootPath)
		if os.IsNotExist(err) {
			os.MkdirAll(rootPath, os.ModePerm)
		} else if err != nil {
			fmt.Println(err)
			return
		}
		factory = &filedriver.FileDriverFactory{
			rootPath,
			perm,
		}
	} else if typ == "gitlab.com/tromos/tromos-ce/engine" {
		log.Admin("Start with gitlab.com/tromos/tromos-ce/engine driver")

		manifest, _ := cfg.GetValue("gitlab.com/tromos/tromos-ce/engine", "manifest")
		bs, _ := cfg.Int("gitlab.com/tromos/tromos-ce/engine", "blocksize")

		factory = driver.TromosDriverFactory{
			Manifest:  manifest,
			Blocksize: bs,
		}.Init()

	} else {
		panic("no driver type input")
	}

	// start web manage UI
	useweb, _ := cfg.Bool("web", "enable")
	if useweb {
		//web.DB = *auth
		web.Perm = perm
		web.Factory = factory
		weblisten, _ := cfg.GetValue("web", "listen")
		admin, _ := cfg.GetValue("admin", "user")
		pass, _ := cfg.GetValue("admin", "pass")
		tls, _ := cfg.Bool("web", "tls")
		certFile, _ := cfg.GetValue("web", "certFile")
		keyFile, _ := cfg.GetValue("web", "keyFile")

		go web.Web(weblisten, "static", "templates", admin, pass, tls, certFile, keyFile)
	}

	ftpName, _ := cfg.GetValue("server", "name")
	opt := &server.ServerOpts{
		Name:    ftpName,
		Factory: factory,
		Port:    port,
		Auth:    &server.SimpleAuth{"paparia", "mentoles"},
	}

	// start ftp server
	ftpServer := server.NewServer(opt)
	log.User("Tromos FTP Server ")
	err = ftpServer.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
