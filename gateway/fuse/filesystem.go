package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
)

var storage *middleware.Client

type FilesystemConfig struct {
	Conn   *fuse.Conn
	Client *middleware.Client
}

func Mount(conf FilesystemConfig) error {

	storage = conf.Client

	if p := conf.Conn.Protocol(); !p.HasInvalidate() {
		panic("Kernel fuse support is too old to have invalidation")
	}

	srv := fs.New(conf.Conn, &fs.Config{
		/*
			Debug: func(msg interface{}) {
				log.Admin(msg)
			},
		*/
	})

	return srv.Serve(&Filesystem{
		RootNode: &Directory{
			name:     "root",
			children: make(map[string]interface{}),
		},
	})
}

type Filesystem struct {
	RootNode *Directory
}

// To be used by the main.go of the library
func (fs *Filesystem) Root() (fs.Node, error) {
	return fs.RootNode, nil
}
