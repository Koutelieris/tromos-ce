package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

// Convert Tromos errors to Fuse errors
func MaskError(err error) error {
	switch err {
	case nil:
		return nil

	case coordinator.ErrBackend:
		return fuse.ENOSYS

	case coordinator.ErrNoImpl:
		return fuse.ENOSYS

	case coordinator.ErrKey:
		return fuse.EIO

	case coordinator.ErrNoExist:
		return fuse.ENOENT

	default:
		log.Admin("Uncaptured error:", err)
		return err
	}
}
