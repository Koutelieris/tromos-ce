package uuid

import (
	"github.com/google/uuid"
)

func Once() string {
	return uuid.New().String()
}
