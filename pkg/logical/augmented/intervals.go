package augmented

import (
	"encoding/binary"
	"fmt"
	"github.com/workiva/go-datastructures/augmentedtree"
)

type dimension struct {
	low, high int64
}

type interval struct {
	dimensions []*dimension
	id         uint64
}

func (i *interval) CheckDimension(dimension uint64) {
	if dimension > uint64(len(i.dimensions)) {
		panic(fmt.Sprintf(`Dimension :%d out of range.`, dimension))
	}
}

func (i *interval) LowAtDimension(dimension uint64) int64 {
	return i.dimensions[dimension-1].low
}

func (i *interval) HighAtDimension(dimension uint64) int64 {
	return i.dimensions[dimension-1].high
}

/* Partial overlap */
func (i *interval) OverlapsAtDimension(iv augmentedtree.Interval, dimension uint64) bool {
	return i.HighAtDimension(dimension) >= iv.LowAtDimension(dimension) &&
		i.LowAtDimension(dimension) <= iv.HighAtDimension(dimension)
}

func (i *interval) ID() uint64 {
	return i.id
}

func (i *interval) Key() []byte {
	key := make([]byte, 8)
	binary.LittleEndian.PutUint64(key, i.id)
	return key
}

func constructSingleDimension(low, high int64, id uint64) *interval {
	return &interval{[]*dimension{{low: low, high: high}}, id}
}
