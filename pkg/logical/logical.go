package logical

import (
	"gitlab.com/tromos/tromos-ce/hub/logical"
)

type DefaultLogical struct {
	length uint64
}

func (df *DefaultLogical) Add(offset int64, size int64) uint64 {
	if uint64(size) > df.length {
		df.length = uint64(size)
	}
	return 0
}

func (df *DefaultLogical) Overlaps(offset int64, bytes int64) (size uint64, segments []logical.Segment) {

	return df.length, []logical.Segment{{To: int64(df.length)}}
}

func (df *DefaultLogical) Length() int64 {
	return int64(df.length)
}
