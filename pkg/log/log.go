package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
	"strings"
)

var ShowAdmin bool = true
var ShowUser bool = true
var ShowTrace bool = false

var logger = logrus.New()

// Fields wraps logrus.Fields, which is a map[string]interface{}
type Fields logrus.Fields

func init() {
	//logger.Formatter = new(logrus.JSONFormatter)
	logger.Formatter = new(logrus.TextFormatter)
	logger.Level = logrus.DebugLevel
	logrus.SetOutput(os.Stdout)
}

func FileInfo(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if !ok {
		file = "<???>"
		line = 1
	} else {
		slash := strings.LastIndex(file, " tromos")
		if slash >= 0 {
			file = file[slash+1:]
		}
	}
	return fmt.Sprintf("%s:%d", file, line)
}

/* Issues related to the administrator. This may include
 * connection errors, bad lookups ... */
func Admin(args ...interface{}) {
	if ShowAdmin {
		entry := logger.WithFields(logrus.Fields{})
		entry.Data["file"] = FileInfo(2)
		entry.Error(args...)
	}
}

/* Issues related to the User. This may include
 * who started a transaction, when, how many data ... */
func User(args ...interface{}) {
	if ShowUser {
		entry := logger.WithFields(logrus.Fields{})
		entry.Data["file"] = FileInfo(2)
		entry.Info(args...)
	}
}

/* Whatever doesn't match the previous categories
 * goes here */
func Trace(args ...interface{}) {
	if ShowTrace {
		entry := logger.WithFields(logrus.Fields{})
		entry.Debug(args...)
	}
}
