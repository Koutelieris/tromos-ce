package debug

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"reflect"
	"runtime"
	"time"
)

// Use it in defer
func TimeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Printf("%s took %s\n", name, elapsed)
}

func Analyze(i interface{}) {
	spew.Dump(i)
}

/* Print the caller location */
func WhereAmI(depthList ...int) string {
	var depth int
	if depthList == nil {
		depth = 1
	} else {
		depth = depthList[0]
	}
	function, _, _, _ := runtime.Caller(depth)
	return runtime.FuncForPC(function).Name()
}

func NameOf(f interface{}) string {
	v := reflect.ValueOf(f)
	if v.Kind() == reflect.Func {
		if rf := runtime.FuncForPC(v.Pointer()); rf != nil {
			return rf.Name()
		}
	}
	return v.String()
}
