package concurrency

import "sync"

func NewAtomicSetter() *AtomicSetter {
	return &AtomicSetter{
		finalCh: make(chan struct{}),
	}
}

type AtomicSetter struct {
	locker  sync.Mutex
	final   bool
	finalCh chan struct{}
}

func (a *AtomicSetter) IsFinal() bool {
	a.locker.Lock()
	defer a.locker.Unlock()

	return a.isFinal()
}

func (a *AtomicSetter) isFinal() bool {
	select {
	case <-a.finalCh:
		return true
	default:
		return false
	}

}

func (a *AtomicSetter) Set(protected func()) {
	a.locker.Lock()
	defer a.locker.Unlock()

	if a.isFinal() {
		panic("Setting value on finalized context")
	}
	protected()
}

func (a *AtomicSetter) Finalize(protected func()) {

	a.locker.Lock()
	defer a.locker.Unlock()

	if a.final {
		panic("Value has been already finalized")
	}

	if protected != nil {
		protected()
	}

	a.final = true
	defer close(a.finalCh)
}

func (a *AtomicSetter) WaitFinal() {
	<-a.finalCh
}
