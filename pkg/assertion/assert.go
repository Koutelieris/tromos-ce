package assertion

import (
	"fmt"
)

func Must(err error) {
	if err != nil {
		panic(err)
	}
}

// _assert will panic with a given formatted message if the given condition is false.
func Assert(condition bool, msg string, v ...interface{}) {
	if !condition {
		panic(fmt.Sprintf("assertion failed: "+msg, v...))
	}
}
