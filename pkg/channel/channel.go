package utils

import (
	"sync/atomic"
)

func MergeAtomic(cs ...<-chan int) <-chan int {
	out := make(chan int)
	var i int32
	atomic.StoreInt32(&i, int32(len(cs)))
	for _, c := range cs {
		go func(c <-chan int) {
			for v := range c {
				out <- v
			}
			if atomic.AddInt32(&i, -1) == 0 {
				close(out)
			}
		}(c)
	}
	return out
}
