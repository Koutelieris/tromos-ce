package path

import (
	"github.com/docker/docker/pkg/fileutils"
	//	"github.com/docker/docker/pkg/idtools"
	//"golang.org/x/sys/unix"
	"os"
	"path/filepath"
)

const FilePathSeparator = string(filepath.Separator)

// Handle some relative paths
func Normalize(path string) string {
	path = filepath.Clean(path)

	switch path {
	case ".":
		return FilePathSeparator
	case "..":
		return FilePathSeparator
	case "*":
		return ""
	default:
		return path
	}
}

func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// EnsurePath returns the given directory. If it doesn't exist, it is created.
// If it exists, its content remain untouched.
func EnsurePath(path string) error {
	if path == "" {
		return nil
	}

	ok, err := Exists(path)
	if err != nil {
		return err
	}
	if ok {
		return nil
	}

	if err := fileutils.CreateIfNotExists(path, true); err != nil {
		return err
	}
	/*
		if err := unix.Access(path, unix.W_OK); err != nil {
			return err
		}
	*/
	return nil
}

/*
// PrepareDir prepares and returns the default directory to for temporary files.
// If it doesn't exist, it is created. If it exists, its content is removed.
func PrepareDir(rootDir string, subdir string) (string, error) {
	idMapping, err := idtools.NewIdentityMapping("tromos", "tromos")
	if err != nil {
		return "", err
	}

	rootIdentity := idMapping.RootPair()

	var tmpDir string
	tmpDir = filepath.Join(rootDir, subdir)
	newName := tmpDir + "-old"
	if err := os.Rename(tmpDir, newName); err == nil {
		go func() {
			if err := os.RemoveAll(newName); err != nil {
				panic(err)
			}
		}()
	} else if !os.IsNotExist(err) {
		if err := os.RemoveAll(tmpDir); err != nil {
			return "", err
		}
	}

	// We don't remove the content of tmpdir if it's not the default,
	// it may hold things that do not belong to us.
	return tmpDir, os.MkdirAll(tmpDir, 0700)
}
*/
