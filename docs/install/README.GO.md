Installing Go
====

`Tromos` is written in Go (http://golang.org), a modern, compiled, statically typed,
concurrent language. This document describes how to build `moclos` from source.

`Tromos's` source code currently depends on Go 1.11. One of the easiest ways
to install golang is from a snap. You may need to first install
the [snap client](https://snapcraft.io/docs/core/install). Installing the golang
snap package is then as easy as

    snap install go --channel=1.11/stable --classic

You can read about the "classic" confinement policy [here](https://insights.ubuntu.com/2017/01/09/how-to-snap-introducing-classic-confinement/)

If you want to use `apt`, then you can add the [Golang Gophers PPA](https://launchpad.net/~gophers/+archive/ubuntu/archive) and then install by running the following

    sudo add-apt-repository ppa:gophers/archive
    sudo apt-get update
    sudo apt install golang-1.11

Alternatively, you can always follow the official [binary installation instructions](https://golang.org/doc/install#install)

Setting GOPATH
--------------

When working with the source of Go programs, you should define a path within
your home directory (or other workspace) which will be your `GOPATH`. `GOPATH`
is similar to Java's `CLASSPATH` or Python's `~/.local`. `GOPATH` is documented
online at `http://golang.org/pkg/go/build/` and inside the `go` tool itself

    go help gopath

Various conventions exist for naming the location of your `GOPATH`, but it should
exist, and be writable by you. For example

    export GOPATH=${HOME}/work
    mkdir $GOPATH

will define and create `$HOME/work` as your local `GOPATH`. The `go` tool itself
will create three subdirectories inside your `GOPATH` when required; `src`, `pkg`
and `bin`, which hold the source of Go programs, compiled packages and compiled
binaries, respectively.

Setting `GOPATH` correctly is critical when developing Go programs. Set and
export it as part of your login script.

Add `$GOPATH/bin` to your `PATH`, so you can run the go programs you install:

    PATH="$GOPATH/bin:$PATH"

