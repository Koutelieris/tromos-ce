= Tutorial for building storage containers

In this tutorial I will try to explain the challenges on building a custom storage system.

== Get the framework and prepare the plugins


To start this tutorial, first we must have the Tromos framework installed into our systems.
But even before that, you need to have link:https://golang.org/doc/install[Go] properly installed on your machine.

Once this is done, clone the Tromos repository and compile it using the following commands

    go get gitlab.com/tromos/tromos-ce/...

`make` will build the binaries for tromos and all the plugins that are available in the link:https://gitlab.com/tromos/tromos-ce/tree/master/hub/README.adoc[HUB]
The source for any dependent packages will also be available inside `$GOPATH`. 
You can use `git pull --rebase`, or the less convenient `go get -u gitlab.com/tromos/tromos-ce/...` to update the source from time to time.



== Using Tromos

In the folder $GOPATH/examples you will several existing templates of storage containers that you can adapt for your case.
However, given that this is a tutorial, let's start by creating a tutorial.yml in which we will describe the specifics for our storage container.


    touch ./examples/tutorial.yml


We start with a very simple model with two Devices. The first device (dev0) uses as backend a directory in the local
filesystem. The second device (dev1) uses on its backend a googledrive. Similarly, any other backend can be easily used.

[source, yaml]
----
Devices:
    "dev0":
        Persistent:
            plugin: "filesystem"
            family: "os"
            path: "/tmp/scratch/hdd0"

    "dev1":
        Persistent:
            plugin: "googledrive"
            credentials: "~/credentials.json"
----



The `Persistent` layer is a connector to external data-stores where our data will be persistently stored. The only mandatory field is the `plugin`, whereas all the other field are calling arguments to the respective plugin. But ...
wait ! how do I know the arguments for the specific plugin ? Currently, the only available way is to read the
documentation of the plugin in the link:https://gitlab.com/tromos/tromos-ce/hub/README.adoc[HUB]. This is because Tromos is oblivious to the plugin's internals - arguments are passed directly from the user to the constructor of the appropriate plugin

Now, we can execute this manifest and mount the respective storage container by running
    
    tromos-cli gateway fuse --manifest ./examples/tutorial.yml --mountpoint /tmp/tutorial

Oh ! Surprise ! Or not ? We have defined the data plane for our virtual storage but we have not defined anything about the metadata plane. Apart from the Devices we must also describe the Coordinators. To do so we append the following definitions into the manifest

[source, yaml]
----
Coordinators:
    "coord0":
        Persistent:
            plugin: "boltdb"
            path: "/tmp/databases/gzone"
----

As you can see the syntax for the devices and coordinators are similar. They are both a stack of layers. The first layer is the `Persistent` which defines where the data (for devices) and metadata (for coordinators) will be stored. The second layer is the `Translator` which handle the requests before reaching the `Persistent` layer. This way, we can perform several optimizations and customizations. For example, we can equip devices with a `Burst-buffer` translator so to aggregate small-sized requests into large blobs before flushing them to the backend. Similarly for coordinators, by controlling the way requests access the backend we can provide consistent on-demand.

The next snippet shows such an example. Note in `dev1` that the *translators are stackable*. For more details link:http://www.tromos.io/docs/overview/introduction/[Click here] 


[source, yaml]
----
Devices:
    "dev0":
        Persistent:
            plugin: "filesystem"
            family: "os"
            path: "/tmp/scratch/hdd0"

        Translators:
            "0":
                plugin: "blob"
                blocksize: 2M
    "dev1":
        Persistent:
            plugin: "googledrive"
            credentials: "~/credentials.json"
        Translators:
            "0":
                plugin: "throttler"
                rate: 500MB
                capacity: 1B
                regulate: channel
            "1":
                plugin: "blob"
                blocksize: 2M

Coordinators:
    "coord0":
        Persistent:
            plugin: "boltdb"
            path: "/tmp/databases/gzone"
        Translators:
            "0":
                plugin: "sequencer"
                blockw2r: true
                blockw2w: true

----

Apparently, different backends require different kind of treatment. For example, because dev1 is backend by a remote Cloud Storage platform it is advisable to
add a burst buffer translator (blob) so to accumulate several small requests into a larger one. In the same manner, we may also need a throttler translator to 
shape the I/O traffic in order to comply with the Googledriver's constraints. Another translator that may be useful is the chunker - so to break incoming traffic into
smaller chunks that meet the maximum support file constraint.

Once we have everything in place we can rerun the `tromos-cli` command and mount the storage container as normal filesystem


== Middleware

But ... wait again ! How is the client going to know which device to use ? Similarly to how Device section describes the layers for a device, the Middleware section describes all the layers of the client middleware. 
Under the hood, Tromos set the configuration to the default following default. The DeviceManager chooses one of the available devices using a random selector, and the Namespace manager chooses
of the availabe coordinators using the consistenthash selector. It must be noted that *the selector plugin for the Namespace manager must be deterministic* - for a given filename it should
always return the same Coordinator. Otherwise, it is impossible to find the right Coordinator to retrieve metadata from. Such deterministic selectors are the 
link:https://gitlab.com/tromos/tromos-ce/hub/selector/consistenthash/README.adoc[consistenthash] or the link:https://gitlab.com/tromos/tromos-ce/hub/selector/radix/README.adoc[radix].
Oppositely, the DeviceManager should distribute the load among several Device. Hence, selector plugins such as link:https://gitlab.com/tromos/tromos-ce/hub/selector/random/README.adoc[random] or
link:https://gitlab.com/tromos/tromos-ce/hub/selector/roundrobin/README.adoc[roundrobin] are preferred.


[source, yaml]
----
Middleware:
    DeviceManager:
        plugin: random
    Namespace:
        plugin: consistenthash
----

For more advanced scenarios where we have Devices or Coordinators with distinct characteristics that we want to take advantage of them, we can use the 
link:https://gitlab.com/tromos/tromos-ce/hub/selector/byqos/README.adoc[byqos] selector. It parses the exposed capabilities of the Devices (or Coordinators) and
creates pools of similarly characteristed resources. For more information link:https://gitlab.com/tromos/tromos-ce/blob/master/docs/tutorial/byqos.adoc[click here]













the application architecture can include information about the DeviceManager, the Namespace, and many others that will be discussed later.
