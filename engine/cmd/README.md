Tromos Daemon (tromosd) is the persistent process that manages Microservices

#### On Linux

The default location of the configuration file on Linux is /etc/tromos/tromos.yml. The --config-file flag can be used to specify a non-default location.
