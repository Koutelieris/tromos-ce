package main // import "gitlab.com/tromos/tromos-ce/engine/cmd"

import (
	"github.com/spf13/cobra"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

func newDaemonCommands() (*cobra.Command, *cobra.Command, error) {

	var tromosd *peer.Peer

	startCmd := &cobra.Command{
		Use:          "start  [OPTIONS]",
		Short:        "run the daemon",
		SilenceUsage: true,
		//SilenceErrors: true,
		//DisableFlagsInUseLine: true,
		//Args:                  cli.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {

			peer, err := peer.New()
			if err != nil {
				return err
			}
			tromosd = peer

			if err := peer.RunWebservice(); err != nil {
				return err
			}

			log.User("Daemon succesfully terminated")
			return nil
		},
	}

	// Link the startCommand with the daemon cli
	RootCmd.AddCommand(startCmd)

	stopCmd := &cobra.Command{
		Use:          "stop  [OPTIONS]",
		Short:        "stop the daemon",
		SilenceUsage: true,
		//SilenceErrors: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			if tromosd != nil {
				return tromosd.Shutdown()
			}
			return nil
		},
	}

	// Link the stopCommand with the daemon cli
	//RootCmd.AddCommand(stopCmd)

	return startCmd, stopCmd, nil
}
