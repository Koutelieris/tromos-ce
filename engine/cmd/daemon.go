package main // import "gitlab.com/tromos/tromos-ce/engine/cmd"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"os/signal"
)

func init() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	// initial log formatting; this setting is updated after the daemon configuration is loaded.
}

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:                   "tromosd [OPTIONS]",
	Short:                 "Tromos standalone command line tool",
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	// Uncomment the following line if your bare application
	// has an action associated with it:
}

func main() {

	startCmd, stopCmd, err := newDaemonCommands()
	if err != nil {
		logrus.Fatal(err)
	}

	// When the daemon is running on the foreground, the user can
	// terminate it with ctrl+c. When running on the background,
	// the user can terminate the daemon through daemon stop command
	errChan := make(chan error, 1)
	go func() {
		defer close(errChan)
		if err := startCmd.Execute(); err != nil {
			errChan <- err
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block waiting either an error to occur to a termination signal to come
	select {
	case <-c:
		signal.Reset(os.Interrupt)
		if err := stopCmd.Execute(); err != nil {
			logrus.Fatal("Daemon forcibly terminated")
			return
		}

	case err := <-errChan:
		if err != nil {
			return
		}
	}
}
