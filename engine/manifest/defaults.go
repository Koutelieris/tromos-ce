package manifest

const (
	DefaultNamespace     = "gitlab.com/tromos/tromos-ce/hub/selector/consistenthash"
	DefaultDeviceManager = "gitlab.com/tromos/tromos-ce/hub/selector/random"
)

var (
	DefaultProcessorManager = []byte(`
--- 
plugin: "gitlab.com/tromos/tromos-ce/hub/selector/random"
`)

	DefaultProcessor = []byte(`
--- 
Capabilities: ["default"]
Graph:  
  plugin: "gitlab.com/tromos/tromos-ce/hub/processor/direct"
`)
)
