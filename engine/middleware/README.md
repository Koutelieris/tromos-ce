

## Datapath
Is the end-to-end path from the client to the devices
client - processor = processor = devices

In the local datapath all the processors are running locally


OnlyLocalDapath indicated that all the Processors are running locally 
Otherwise, see Authority Peer (from device manager)


	// Optionally make the query functions of the device manager accessible through the network
	// This is need when the Authority Device Manager is other than the local Device Manager
	// (the case for datapaths that span across several nodes)

