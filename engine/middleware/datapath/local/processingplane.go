package datapath

import (
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gopkg.in/go-playground/validator.v9"
)

// LocalDatapath connect local processor with local device manager
type LocalDatapath struct {
	config   datapath.DatapathConfig
	discover chan *datapath.Discovery
	network  *goflow.Graph
	done     goflow.Wait
}

func NewLocalDatapath(opts ...datapath.DatapathOption) (*LocalDatapath, error) {

	config := datapath.DatapathConfig{}
	for _, opt := range opts {
		opt(&config)
	}

	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	proc, err := config.ProcessorManager.SelectAndReserve()
	if err != nil {
		return nil, err
	}

	pplane := &ProcessPlane{Processor: proc}
	splane := &StoragePlane{}

	// Init IOdiscover network (the custom graph that will be used)
	network := goflow.NewGraph()
	network.Add("InTransit", pplane)
	network.Add("Storage", splane)
	network.MapInPort("In", "InTransit", "In")
	network.Connect("InTransit", "Out", "Storage", "In")

	discover := make(chan *datapath.Discovery)
	network.SetInPort("In", discover)

	done := goflow.Run(network)

	return &LocalDatapath{
		config:   config,
		discover: discover,
		network:  network,
		done:     done,
	}, nil
}

func (_ *LocalDatapath) String() string {
	return "LocalDatapath"
}

func (_ *LocalDatapath) Reusable() bool {
	return false
}

func (_ *LocalDatapath) Capabilities() []selector.Capability {
	return nil
}

func (_ *LocalDatapath) Location() string {
	return ""
}

func (path *LocalDatapath) Close() error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	<-path.done
	// FIXME: Should I use it ?
	//close(path.discover)
	//<-path.network.Wait()
	return nil
}

func (path *LocalDatapath) NewChannel(opts ...processor.ChannelOption) (processor.Channel, error) {

	var config processor.ChannelConfig
	for _, opt := range opts {
		opt(&config)
	}
	return path.NewChannelFromConfig(config)
}

func (path *LocalDatapath) NewChannelFromConfig(config processor.ChannelConfig) (processor.Channel, error) {

	// TODO: if ever migrate to distributedDatapath, do not forget to set the
	// Authority Device Manager to the local device manager of the client
	// who initiates the channel (or to the central Authority Device Manager, if any)
	// To do so, use the cluster-facing ip into the WalkID
	request := &datapath.Discovery{
		DeviceManager: path.config.DeviceManager,
		ChannelConfig: config,
	}

	reservation, err := datapath.Discover(path.discover, request, "127.0.0.1")
	if err != nil {
		return nil, err
	}

	return &channel{
		request:  request,
		response: reservation,
	}, nil

}

type channel struct {
	request  *datapath.Discovery
	response *datapath.Reservation
}

func (ch *channel) NewTransfer(stream *processor.Stream) error {
	ch.response.Listener <- stream
	return nil
}

func (ch *channel) Close() error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	datapath.WaitRelease(ch.response)
	return nil
}

func (ch *channel) Capabilities(port string) []selector.Capability {
	panic("Should not be called")
}

func (ch *channel) Sinks() map[string]string {
	return ch.request.ChannelConfig.Sinks
}

func (ch *channel) Readyports() chan *processor.Stream {
	panic("Should not be called")
}

type ProcessPlane struct {
	goflow.Component
	In  <-chan *datapath.Discovery
	Out chan<- *datapath.Discovery

	processor.Processor
}

func (plane *ProcessPlane) Process() {
	for req := range plane.In {
		go newReservation(req, plane.Processor, plane.Out)
	}
}

func newReservation(req *datapath.Discovery, processor processor.Processor, outs chan<- *datapath.Discovery) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	ch, err := processor.NewChannelFromConfig(req.ChannelConfig)
	if err != nil {
		datapath.Abort(req, err)
		return
	}
	defer ch.Close()

	// discover and reserve links with the components connected to the Processor's outputs
	nextHops := make(map[string]*datapath.Reservation)
	for port := range ch.Sinks() {

		reservation, err := datapath.Discover(outs, req, port, ch.Capabilities(port)...)
		if err != nil {
			datapath.Abort(req, err)
			return
		}
		nextHops[port] = reservation
	}

	reservation := datapath.Reserve(req)
	defer datapath.Release(reservation)

	for in := range reservation.Listener {
		if err := ch.NewTransfer(in); err != nil {
			datapath.Abort(req, err)
			return
		}
		for outs := range ch.Readyports() {
			// Forward the stream as soon as its ready
			nextHops[outs.Meta.Port].Listener <- outs
		}
	}

	//	Terminate processor
	for _, nexthop := range nextHops {
		datapath.WaitRelease(nexthop)
	}
}
