package datapath

import (
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/processor"
)

type transfer struct {
	in  *processor.Stream
	out *device.Stream
}

type StoragePlane struct {
	goflow.Component
	In  <-chan *datapath.Discovery
	Out chan<- *datapath.Discovery
}

func (sp *StoragePlane) Process() {
	for req := range sp.In {
		if req.ChannelConfig.Writable {
			go Upstream(req)
		} else {
			go Downstream(req)
		}
	}
}

func Upstream(req *datapath.Discovery) {
	// datapath - Create channels with devices
	dev, err := req.DeviceManager.Reserve(
		datapath.Authority(req),
		req.ChannelConfig.Name,
		req.Capabilities...,
	)
	if err != nil {
		datapath.Abort(req, err)
		return
	}

	channel, err := dev.NewWriteChannel(req.ChannelConfig.Name)
	if err != nil {
		datapath.Abort(req, err)
		return
	}
	req.ConfigLocker.Lock()
	req.ChannelConfig.Sinks[datapath.WalkID(req)] = dev.String()
	req.ConfigLocker.Unlock()

	reservation := datapath.Reserve(req)
	defer datapath.Release(reservation)

	// And run (I/O Streams)
	var transfers []*transfer
	for in := range reservation.Listener {
		transfer := &transfer{
			in:  in,
			out: &device.Stream{Complete: make(chan struct{})},
		}

		reader := transfer.in.Data.(processor.Receiver).PipeReader
		if err := channel.NewTransfer(reader, transfer.out); err != nil {
			panic(err)
		}
		transfers = append(transfers, transfer)
	}

	if err := channel.Close(); err != nil {
		panic(err)
	}

	for _, transfer := range transfers {
		transfer.in.Meta.Items[datapath.WalkID(req)] = transfer.out.Item
	}
}

func Downstream(req *datapath.Discovery) {

	deviceID := req.ChannelConfig.Sinks[datapath.WalkID(req)]

	dev, err := req.DeviceManager.GetDevice(deviceID)
	if err != nil {
		datapath.Abort(req, err)
		return
	}

	channel, err := dev.NewReadChannel(req.ChannelConfig.Name)
	if err != nil {
		datapath.Abort(req, err)
		return
	}

	reservation := datapath.Reserve(req)
	defer datapath.Release(reservation)

	for parent := range reservation.Listener {
		// It is possible that data are contained only in a subset of the devices.
		// This can be either due to scan where data are located only
		// on a specific device and the rest are nil, or like strip
		// where dataholders have been created but not populated with data
		item, ok := parent.Meta.Items[datapath.WalkID(req)]
		if !ok || item.IsEmpty() {
			// Terminate the writer. No need to go further into the storage line
			parent.Data.Close()
		} else {
			writer := parent.Data.(processor.Sender).PipeWriter
			err := channel.NewTransfer(writer, &device.Stream{Item: item})
			if err != nil {
				if err := writer.CloseWithError(err); err != nil {
					panic(err)
				}
			}
		}
	}

	if err := channel.Close(); err != nil {
		panic(err)
	}
}
