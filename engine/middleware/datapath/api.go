package datapath // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	//"gitlab.com/tromos/tromos-ce/pkg/log"
	"strings"
	"sync"
)

type ProcessorManager interface {
	Close() error
	SelectAndReserve(capabilities ...selector.Capability) (processor.Processor, error)
	GetProcessor(processorID string) (processor.Processor, error)
}

type DeviceManager interface {
	Close() error
	Reserve(authority string, tid string, capabilities ...selector.Capability) (device.Device, error)
	SelectAndReserve(tid string, capabilities ...selector.Capability) (string, error)
	GetDevice(deviceID string) (device.Device, error)
}

type DatapathConfig struct {
	ProcessorManager ProcessorManager `validate:"required"`
	DeviceManager    DeviceManager    `validate:"required"`
}

type DatapathOption func(*DatapathConfig)

// Processor defines the local processor for the node
func WithProcessorManager(pm ProcessorManager) DatapathOption {
	return func(c *DatapathConfig) {
		c.ProcessorManager = pm
	}
}

// DeviceManager defines the local device manager for the node
func WithDeviceManager(dm DeviceManager) DatapathOption {
	return func(c *DatapathConfig) {
		c.DeviceManager = dm
	}
}

func WalkID(req *Discovery) string {
	return strings.Join(req.walkID, ".")
}

func Authority(req *Discovery) string {
	return req.walkID[0]
}

// The first element of the WalkID is the authority Device Manager
type Discovery struct {
	DeviceManager DeviceManager `validate:"required"`

	ConfigLocker  sync.Mutex
	ChannelConfig processor.ChannelConfig `validate:"required"`

	Capabilities []selector.Capability
	waitresponse chan interface{}
	walkID       []string `validate:"required"`
}

func Discover(nexthop chan<- *Discovery, current *Discovery, port string, capabilities ...selector.Capability) (*Reservation, error) {

	waitresponse := make(chan interface{})

	// Create links with the elements on the outputs
	// Remember to expand the capabilities, otherwise it will be placed as
	// []interface{} and will screw the capability parsing
	nexthop <- &Discovery{
		DeviceManager: current.DeviceManager,
		ChannelConfig: current.ChannelConfig,
		Capabilities:  append(current.Capabilities, capabilities...),
		walkID:        append(current.walkID, port),
		waitresponse:  waitresponse,
	}

	resp := <-waitresponse
	switch resp.(type) {
	case error:
		return nil, resp.(error)
	case *Reservation:
		return resp.(*Reservation), nil
	default:
		panic("Unknown datapath response type")
	}
}

func Reserve(req *Discovery) *Reservation {
	reservation := &Reservation{
		Listener: make(chan *processor.Stream, viper.GetInt("MaxConcurrentStreamsPerChannel")),
		alive:    make(chan struct{}),
	}
	req.waitresponse <- reservation
	close(req.waitresponse)
	return reservation
}

func Abort(req *Discovery, err error) {
	req.waitresponse <- err
	close(req.waitresponse)
}

type Reservation struct {
	Listener chan *processor.Stream // Closing listener : top-bottom
	alive    chan struct{}          // Closing report: bottom-up
}

// Release only when all incoming streams have finished
func Release(resp *Reservation) {
	close(resp.alive)
}

func WaitRelease(resp *Reservation) {
	close(resp.Listener)
	<-resp.alive
}
