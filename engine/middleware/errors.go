package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrMiddleware = errors.NewClass("Device Error")
	ErrArg        = ErrMiddleware.NewClass("Argument error")
	ErrRuntime    = ErrMiddleware.NewClass("Runtime error")

	// Generic family errors
	ErrBackend           = ErrMiddleware.New("Backend error")
	ErrOnlyLocalDatapath = ErrMiddleware.New("Only local datapath is supported for the momenet")

	// Argument family errors
	ErrInvalid = ErrArg.New("Invalid argument")

	// Runtime family errors
	ErrStaleCluster = ErrRuntime.New("Stale cluster information")
)
