package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
	local "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/local"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gopkg.in/go-playground/validator.v9"
)

type ClientConfig struct {
	Manifest *manifest.Manifest `validate:"required"`

	// LocalDatapath indicate all the Processors are running locally
	LocalDatapath bool
}

func NewClient(config *ClientConfig) (*Client, error) {
	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	// Join the virtual infrastructure and initiate whatever Microservices and
	// connectors are meant to be used on the local peer
	peer, err := peer.New()
	if err != nil {
		return nil, err
	}

	if err := peer.Bootstrap(config.Manifest); err != nil {
		return nil, err
	}

	// Configure Metadata mesh
	selector, conf := config.Manifest.NamespaceConfiguration()
	plugin, err := peer.OpenSelectorPlugin(selector)
	if err != nil {
		return nil, err
	}
	nm, err := NewNamespace(&NamespaceConfig{
		Peer:     peer,
		Selector: plugin(conf),
	})
	if err != nil {
		return nil, err
	}

	// Configure Data mesh
	selector, conf = config.Manifest.DeviceManagerConfiguration()
	plugin, err = peer.OpenSelectorPlugin(selector)
	if err != nil {
		return nil, err
	}
	dm, err := NewDeviceManager(&DeviceManagerConfig{
		Peer:     peer,
		Selector: plugin(conf),
	})
	if err != nil {
		return nil, err
	}

	// Configure Processing mesh
	selector, conf = config.Manifest.ProcessorManagerConfiguration()
	plugin, err = peer.OpenSelectorPlugin(selector)
	if err != nil {
		return nil, err
	}
	pm, err := NewProcessorManager(&ProcessorManagerConfig{
		Peer:     peer,
		Selector: plugin(conf),
	})
	if err != nil {
		return nil, err
	}

	if !config.LocalDatapath {
		return nil, ErrOnlyLocalDatapath
	}

	// Setup Datapath (Processing + Storage)
	dp, err := local.NewLocalDatapath(
		datapath.WithProcessorManager(pm),
		datapath.WithDeviceManager(dm),
	)
	if err != nil {
		return nil, err
	}

	logrus.Printf("Tromos successfully kickstarted manifest %s", config.Manifest.ID())
	return &Client{
		logger:           logrus.WithField("module", "peer"),
		config:           config,
		DeviceManager:    dm,
		ProcessorManager: pm,
		Namespace:        nm,
		Datapath:         dp,
	}, nil
}

type Client struct {
	logger *logrus.Entry
	config *ClientConfig

	ProcessorManager *ProcessorManager
	DeviceManager    *DeviceManager
	Namespace        *Namespace
	Datapath         processor.Processor
}

func (client *Client) Close() error {
	if err := client.Datapath.Close(); err != nil {
		return err
	}

	if err := client.ProcessorManager.Close(); err != nil {
		return err
	}

	if err := client.DeviceManager.Close(); err != nil {
		return err
	}

	if err := client.Namespace.Close(); err != nil {
		return err
	}
	client.logger.Print("Client middleware succesfully shutdown manifest %s", client.config.Manifest.ID())
	return nil
}

func (client *Client) Remove(key string) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	partition := client.Namespace.Partition(key)
	return partition.SetLandmark(key, coordinator.Landmark{Disappear: true})
}

func (client *Client) Truncate(key string) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	partition := client.Namespace.Partition(key)
	return partition.SetLandmark(key, coordinator.Landmark{IgnorePrevious: true})
}

func (client *Client) CreateOrReset(key string) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	partition := client.Namespace.Partition(key)
	return partition.CreateOrReset(key)
}

func (client *Client) CreateIfNotExist(key string) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	partition := client.Namespace.Partition(key)
	return partition.CreateIfNotExist(key)
}
