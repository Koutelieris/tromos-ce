package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gopkg.in/go-playground/validator.v9"
)

type NamespaceConfig struct {
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

type Namespace struct {
	config *NamespaceConfig
	mesh   map[string]coordinator.Coordinator
}

func NewNamespace(config *NamespaceConfig) (*Namespace, error) {

	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	nm := &Namespace{
		config: config,
		mesh:   make(map[string]coordinator.Coordinator),
	}

	for cid, coord := range config.Peer.Coordinators() {
		nm.mesh[cid] = coord
		nm.config.Selector.Add(selector.SelectorProperties{
			ID:           cid,
			Capabilities: coord.Capabilities(),
			Peer:         coord.Location(),
		})
	}
	nm.config.Selector.Commit()

	return nm, nil
}

// Close shutdowns all the coordinators of the namespace
func (nm *Namespace) Close() error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	nm.config.Selector.Walk(func(k []byte, _ interface{}) bool {
		if err := nm.mesh[string(k)].Close(); err != nil {
			return true
		}
		return false
	})
	return nil
}

// Partition returns the Coordinator resposinble for the key
func (nm *Namespace) Partition(key string) coordinator.Coordinator {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	proposed, err := nm.config.Selector.Partition(key)
	if err != nil {
		panic(err)
	}
	return nm.mesh[proposed]
}
