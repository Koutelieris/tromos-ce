package hub // import "gitlab.com/tromos/tromos-ce/engine/hub"

import (
	"context"
	"crypto/sha256"
	"fmt"
	getter "github.com/hashicorp/go-getter"
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/beautify"
	"gitlab.com/tromos/tromos-ce/pkg/path"
	"os"
	"os/exec"
	"path/filepath"
	"plugin"
	"time"
)

var (
	Timeout time.Duration = 500 * time.Second
	hub     *Hub
)

func init() {
	h, err := NewHub(os.Getenv("GOPATH"))
	if err != nil {
		panic(err)
	}
	hub = h
}

// GetHub returns the default Hub
func GetHub() *Hub {
	return hub
}

// Hub is a local hub, or repository,  of modules and schemas
type Hub struct {
	ctx    context.Context
	path   string
	logger *logrus.Entry
}

// NewHub creates a New hub
func NewHub(pluginDir string) (*Hub, error) {
	if err := path.EnsurePath(pluginDir); err != nil {
		return nil, err
	}

	// Store the environmental variables that will be used
	// by go get when compiling the plugin source code
	//os.Setenv("GOPATH", pluginDir)
	os.Setenv("GOBIN", pluginDir+"/bin")

	return &Hub{
		ctx:    context.Background(),
		path:   pluginDir,
		logger: logrus.WithField("module", "hub"),
	}, nil
}

// shortenPath returns a hash of the path uri. It is primarily create hashes of the
// given uris and maintain the binary plugins in a flat namespace
func (hub *Hub) shortenPath(uri string) string {
	pluginID := fmt.Sprintf("%x", sha256.Sum256([]byte(uri)))

	return hub.path + "/bin/" + pluginID
}

// pluginPkg returns the path of the local source code of the plugin
func (hub *Hub) pluginPkg(uri string) string {
	pluginID := fmt.Sprintf("%x", sha256.Sum256([]byte(uri)))
	return hub.path + "/pkg/" + pluginID
}

// FixDependencies makes sure that all the modules are locally available.
// URIs ending in (.bin) are assumed to be proprietary self-contained binaries that are
// directly downloaded. Otherwise, the plugin source is downloaded and locally compiled
// to produce the plugin binary
func FixDependencies(uris []string) error {
	for _, uri := range uris {
		suffix := filepath.Ext(uri)

		if suffix == "bin" {
			if err := hub.GetPluginBinary(uri); err != nil {
				return err
			}
		}
		if err := hub.GetPluginSource(uri); err != nil {
			return err
		}

	}
	return nil
}

// GetPluginSource uses the go get command to download and compiles the plugin from the default Hub.
// The binary will we storedin GOPATH/bin
func GetPluginSource(uri string) error { return hub.GetPluginSource(uri) }

// GetPluginSource uses the go get command to download and compiles the plugin
// The binary will we stored in GOPATH/bin
func (hub *Hub) GetPluginSource(uri string) error {
	hub.logger.Info("Download and compile plugin ", uri)
	cmd := exec.Command("/usr/bin/go", "get", "-v", "-buildmode=plugin", uri)

	//log.Admin("Env:", os.Environ())
	//cmd.Env = append(os.Environ(), "GOPATH="+hub.path)

	// TODO: link the stdout and stderr to the respective logrus outputs.
	// Until then, omit the command's output
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// GetPluginBinary downloads the plugin binary from the default Hub
func GetPluginBinary(uri string) error { return hub.GetPluginBinary(uri) }

// GetPluginBinary downloads the binary plugin
func (hub *Hub) GetPluginBinary(uri string) error {
	pluginpath := hub.shortenPath(uri)

	if _, err := os.Stat(pluginpath); err == nil {
		hub.logger.Debug("Plugin %s already exists @ %s", uri, pluginpath)
		// Plugin already exists so avoid re-downloading it
		// TODO: version checkking
		return nil
	}

	opts := []getter.ClientOption{getter.WithProgress(&beautify.ProgressBar{})}

	ctx, cancel := context.WithTimeout(hub.ctx, Timeout)
	defer cancel() // releases resources if get operation completed before time elapses

	client := &getter.Client{
		Ctx:     ctx,
		Src:     uri,
		Dst:     hub.shortenPath(uri),
		Mode:    getter.ClientModeFile,
		Options: opts,
	}

	if err := client.Get(); err != nil {
		hub.logger.WithError(err).Warnf("Download error %s", hub.shortenPath(uri))
		return err
	}
	hub.logger.Infof("%s succesfully downloaded @ %s", uri, hub.shortenPath(uri))
	return nil
}

// OpenPlugin opens a plugin on the default Hub. The name of the plugins are expected to be in the form
// type/name (e.g., device/filesystem)
func OpenPlugin(uri string) (interface{}, error) { return hub.OpenPlugin(uri) }

// OpenPlugin opens a plugin. The name of the plugins are expected to be in the form
// type/name (e.g., device/filesystem)
func (hub *Hub) OpenPlugin(uri string) (interface{}, error) {
	binary := hub.shortenPath(uri)
	table, err := plugin.Open(binary)
	if err != nil {
		return nil, err
	}

	symbol, err := table.Lookup("Plugin")
	if err != nil {
		return nil, err
	}

	return symbol, nil
}

// OpenDevicePlugin opens a Device plugin on the default Hub
func OpenDevicePlugin(element string) (device.DevicePlugin, error) {
	return hub.OpenDevicePlugin(element)
}

// OpenDevicePlugin opens a Device plugin
func (hub *Hub) OpenDevicePlugin(element string) (device.DevicePlugin, error) {
	if element == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(element)
	if err != nil {
		return nil, err
	}
	return (*symbol.(*device.DevicePlugin)), nil
}

// OpenCoordinatorPlugin opens a Coordinator plugin on the default Hub
func OpenCoordinatorPlugin(element string) (coordinator.CoordinatorPlugin, error) {
	return hub.OpenCoordinatorPlugin(element)
}

// OpenCoordinatorPlugin opens a Coordinator plugin
func (hub *Hub) OpenCoordinatorPlugin(element string) (coordinator.CoordinatorPlugin, error) {
	if element == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(element)
	if err != nil {
		return nil, err
	}
	return *symbol.(*coordinator.CoordinatorPlugin), nil
}

// OpenProcessorPlugin opens a Processor plugin on the default Hub
func OpenProcessorPlugin(element string) (processor.ProcessorPlugin, error) {
	return hub.OpenProcessorPlugin(element)
}

// OpenCoordinatorPlugin opens a Coordinator plugin
func (hub *Hub) OpenProcessorPlugin(element string) (processor.ProcessorPlugin, error) {
	if element == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(element)
	if err != nil {
		return nil, err
	}

	return *symbol.(*processor.ProcessorPlugin), nil
}

// OpenSelectorPlugin opens a Selector plugin on the default Hub
func OpenSelectorPlugin(element string) (selector.SelectorPlugin, error) {
	return hub.OpenSelectorPlugin(element)
}

// OpenSelectorPlugin opens a Selector plugin
func (hub *Hub) OpenSelectorPlugin(element string) (selector.SelectorPlugin, error) {
	if element == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(element)
	if err != nil {
		return nil, err
	}
	return *symbol.(*selector.SelectorPlugin), nil
}
