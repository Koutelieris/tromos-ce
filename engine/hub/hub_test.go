package hub

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOpenDevicePlugin(t *testing.T) {

	err := Ensure("device/filesystem")
	assert.Nil(t, err)

	plugin, err := OpenDevicePlugin("filesystem")
	assert.Nil(t, err)
	assert.NotNil(t, plugin)
}
