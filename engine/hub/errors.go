package hub // import "gitlab.com/tromos/tromos-ce/engine/hub"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrHub     = errors.NewClass("Device Error")
	ErrArg     = ErrHub.NewClass("Argument error")
	ErrRuntime = ErrHub.NewClass("Runtime error")

	// Generic family errors

	// Argument family errors

	// Runtime family errors
	ErrNotFound = ErrRuntime.New("Plugin not found")
)
