package coordinator // import "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

// Coordinator inherits the properties of the backend
// coordinator.Coordinator and augments it with information of the key partition
// the coordinator is meant to serve
type Coordinator struct {
	coordinator.Coordinator
	identifier   string
	capabilities []selector.Capability
}

func (c *Coordinator) String() string {
	return c.identifier
}

func (c *Coordinator) Capabilities() []selector.Capability {
	return c.capabilities
}
