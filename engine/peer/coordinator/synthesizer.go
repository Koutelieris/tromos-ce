package coordinator // import "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
)

// Synthesizes composes a virtual Coordinator instances by piggybacking layers
// (persistent, translators, proxy)
func Synthesize(cid string, config *viper.Viper) (coordinator.Coordinator, error) {
	if config == nil {
		return nil, coordinator.ErrInvalid
	}

	var layer coordinator.Coordinator
	islocal := net.IsLocalAddress(config.GetString("Proxy.host"))

	// It is local when 1) there is no proxy 2) the proxy ip is a local ip
	if !islocal {
		proxyclient := config.Sub("proxy")
		plugin, err := hub.OpenCoordinatorPlugin(proxyclient.GetString("plugin") + ".client")
		if err != nil {
			return nil, err
		}
		layer = plugin(proxyclient)

		layer.SetBackend(DefaultCoordinator{})
	} else {
		persistent := config.Sub("persistent")

		plugin, err := hub.OpenCoordinatorPlugin(persistent.GetString("plugin"))
		if err != nil {
			return nil, err
		}

		layer = plugin(persistent)
		layer.SetBackend(DefaultCoordinator{})

		// Load intermediate connectors (and change high-order accoringdly)
		stack := structures.SortMapStringKeys(config.GetStringMap("Translators"))
		for _, seqID := range stack {
			translator := config.Sub("translators." + seqID)

			plugin, err := hub.OpenCoordinatorPlugin(translator.GetString("plugin"))
			if err != nil {
				return nil, err
			}

			newlayer := plugin(translator)
			newlayer.SetBackend(layer)
			layer = newlayer
		}

		// Local coordinator exposed through proxy (the proxy ip is the local ip)
		if len(config.GetStringMapString("Proxy")) > 0 {
			proxyserver := config.Sub("proxy")
			plugin, err := hub.OpenCoordinatorPlugin(proxyserver.GetString("plugin") + ".server")
			if err != nil {
				return nil, err
			}

			server := plugin(proxyserver)
			server.SetBackend(layer)
		}
	}

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, cap := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[cap]
		if !ok {
			return nil, coordinator.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	log.User("Found Coordinator ", cid, "@", layer.Location())
	// TODO: String to capabilities
	//viper.GetStringSlice(alias + ".Capabilities"),
	return &Coordinator{
		Coordinator:  layer,
		identifier:   cid,
		capabilities: capabilities,
	}, nil
}
