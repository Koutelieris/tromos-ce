package coordinator // import "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

type DefaultCoordinator struct{}

func (DefaultCoordinator) SetBackend(coordinator.Coordinator) {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) String() string {
	return "DefaultCoordinator"
}

func (DefaultCoordinator) Capabilities() []selector.Capability {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) Location() string {
	return "localhost"
}

func (DefaultCoordinator) Close() error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) Info(key string) ([][]byte, coordinator.Info, error) {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) CreateOrReset(key string) error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) CreateIfNotExist(key string) error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) SetLandmark(key string, mark coordinator.Landmark) error {
	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) UpdateStart(key string, tid string, intentions coordinator.IntentionRecord) error {

	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) UpdateEnd(key string, tid string, record []byte) error {

	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) ViewStart(key string, filter []string) (records [][]byte, tids []string, err error) {

	panic(coordinator.ErrBackend)
}

func (DefaultCoordinator) ViewEnd(tids []string) error {

	panic(coordinator.ErrBackend)
}
