package processor // import "gitlab.com/tromos/tromos-ce/engine/peer/processor"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

// Synthesize composes the Processor as specified in the configuration
func Synthesize(pid string, config *viper.Viper) (processor.Processor, error) {
	if config == nil {
		return nil, processor.ErrInvalid
	}

	graphconfig := config.Sub("graph")
	plugin, err := hub.OpenProcessorPlugin(graphconfig.GetString("plugin"))
	if err != nil {
		return nil, err
	}

	graph := plugin(graphconfig)

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, cap := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[cap]
		if !ok {
			return nil, processor.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	//log.User("Found Processor ", pid, "@", layer.Location())
	log.User("Found Processor ", pid, "@localhost")
	p := &Processor{
		identifier:   pid,
		capabilities: capabilities,
	}
	p.init(graph)

	return p, nil
}
