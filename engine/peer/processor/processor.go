package processor // import "gitlab.com/tromos/tromos-ce/engine/peer/processor"

import (
	"context"
	"github.com/jolestar/go-commons-pool"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	//"gitlab.com/tromos/tromos-ce/pkg/log"
)

type upstream struct {
	pool     *pool.ObjectPool
	generate func() processor.Channel
}

type downstream struct {
	pool     *pool.ObjectPool
	generate func() processor.Channel
}

type channel struct {
	isupstream bool
	*Builder
	p *Processor
}

func (ch *channel) Close() error {
	ctx := context.Background()
	switch {
	case ch.isupstream && ch.p.upstreams.pool != nil:
		return ch.p.upstreams.pool.ReturnObject(ctx, ch.Builder)
	case !ch.isupstream && ch.p.downstreams.pool != nil:
		return ch.p.downstreams.pool.ReturnObject(ctx, ch.Builder)
	default:
		return ch.Builder.Close()
	}
}

type Processor struct {
	identifier   string
	peer         string
	capabilities []selector.Capability
	upstreams    upstream
	downstreams  downstream
}

func (p *Processor) init(graph processor.ProcessGraph) {
	// function for compile at runtime
	p.upstreams.generate = func() processor.Channel {
		return &channel{
			Builder:    newBuilder(true, graph),
			isupstream: true,
			p:          p,
		}
	}
	p.downstreams.generate = func() processor.Channel {
		return &channel{
			Builder:    newBuilder(false, graph),
			isupstream: false,
			p:          p,
		}
	}

	// If not reusable, we cannot use pool
	if graph.Reusable() {
		return
	}

	// generic pool configuration
	uctx := context.Background()
	config := &pool.ObjectPoolConfig{
		LIFO:                 true,
		MaxTotal:             viper.GetInt("MaxConcurrentChannels"),
		MaxIdle:              -1,
		MinEvictableIdleTime: -1,
		BlockWhenExhausted:   false,
	}

	// Set up pools specifics
	p.upstreams.pool = pool.NewObjectPool(uctx,
		pool.NewPooledObjectFactorySimple(func(context.Context) (interface{}, error) {
			return p.upstreams.generate(), nil
		}),
		config,
	)
	pool.Prefill(uctx, p.upstreams.pool, viper.GetInt("MaxConcurrentChannels"))

	p.downstreams.pool = pool.NewObjectPool(uctx,
		pool.NewPooledObjectFactorySimple(func(context.Context) (interface{}, error) {
			return p.downstreams.generate(), nil
		}),
		config,
	)
	pool.Prefill(uctx, p.downstreams.pool, viper.GetInt("MaxConcurrentChannels"))

	// And make the generate to get items frm the pool
	p.upstreams.generate = func() processor.Channel {
		obj, err := p.upstreams.pool.BorrowObject(uctx)
		if err != nil {
			panic(err)
		}
		return obj.(processor.Channel)
	}

	p.downstreams.generate = func() processor.Channel {
		obj, err := p.downstreams.pool.BorrowObject(uctx)
		if err != nil {
			panic(err)
		}
		return obj.(processor.Channel)
	}
}

func (p *Processor) String() string {
	return p.identifier
}

func (p *Processor) Location() string {
	return p.peer
}

func (p *Processor) Capabilities() []selector.Capability {
	return p.capabilities
}

func (p *Processor) NewChannel(opts ...processor.ChannelOption) (processor.Channel, error) {
	config := processor.ChannelConfig{}
	for _, opt := range opts {
		opt(&config)
	}
	return p.NewChannelFromConfig(config)
}

func (p *Processor) NewChannelFromConfig(config processor.ChannelConfig) (processor.Channel, error) {
	if config.Writable {
		return p.upstreams.generate(), nil
	} else {
		return p.downstreams.generate(), nil
	}
}

func (p *Processor) Close() error {
	// Close the pools if they exist. They may not exist though if
	// the graph is not reusable
	if p.upstreams.pool != nil {
		p.upstreams.pool.Close(context.Background())
	}
	if p.downstreams.pool != nil {
		p.downstreams.pool.Close(context.Background())
	}
	return nil
}
