package peer // import "gitlab.com/tromos/tromos-ce/engine/peer"

import (
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"net/http"
)

// RunWebservice makes the peer functionality availablle through RPC
func (peer *Peer) RunWebservice() error {
	ops := rpc.NewWebSocketService()
	ops.AddFunction("Bootstrap", peer.Bootstrap)

	webservice := &http.Server{
		Addr:    peer.address + ":" + WebServicePort,
		Handler: ops,
	}
	peer.webservice = webservice

	errCh := make(chan error)

	go func() {
		peer.logger.Printf("Starting Webservice at %s", webservice.Addr)
		err := webservice.ListenAndServe()
		errCh <- err
		close(errCh)
	}()
	return <-errCh
}
