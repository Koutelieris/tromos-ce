package peer // import "gitlab.com/tromos/tromos-ce/engine/peer"

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	vcoord "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"
	vdev "gitlab.com/tromos/tromos-ce/engine/peer/device"
	hub "gitlab.com/tromos/tromos-ce/engine/hub"
	vproc "gitlab.com/tromos/tromos-ce/engine/peer/processor"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"net/http"
)

const (
	WebServicePort string = "6666"
)

type Peer struct {
	*hub.Hub

	logger      *logrus.Entry
	startupDone chan struct{}

	// Meshes of Instantiated Microservices
	devices      map[string]device.Device
	coordinators map[string]coordinator.Coordinator
	processors   map[string]processor.Processor

	address    string
	webservice *http.Server
}

// New sets up a new peer
func New() (peer *Peer, err error) {

	address, err := net.GetLocalIP()
	if err != nil {
		return nil, err
	}

	return &Peer{
		Hub:          hub.GetHub(),
		logger:       logrus.WithField("module", "peer"),
		startupDone:  make(chan struct{}),
		devices:      make(map[string]device.Device),
		coordinators: make(map[string]coordinator.Coordinator),
		processors:   make(map[string]processor.Processor),
		address:      address,
	}, nil
}

func (peer *Peer) IsRunning() {
	<-peer.startupDone
}

// Address returns the ip address which the peer will use to spawn microservices. The
// address must be the same as in the Manifest
func (peer *Peer) Address() string {
	return peer.address
}

// Shutdown closes the peer and all the associated Microservices running on the node
func (peer *Peer) Shutdown() error {

	// TODO make it atomic
	for _, dev := range peer.devices {
		if err := dev.Close(); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	for _, coord := range peer.coordinators {
		if err := coord.Close(); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	for _, proc := range peer.processors {
		if err := proc.Close(); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	if peer.webservice != nil {
		if err := peer.webservice.Shutdown(context.TODO()); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	return nil
}

// Bootstrap intantiates Microservices according to the manifest
func (peer *Peer) Bootstrap(man *manifest.Manifest) error {

	if err := man.Validate(); err != nil {
		return err
	}

	// Find and download all the plugins for the peer
	pluginList := man.PluginsOnPeer(peer.address)
	if len(pluginList) == 0 {
		peer.logger.Warnf("No plugins were defined for peer ", peer.address)
	}

	// Download all the necessary plugins
	if err := hub.FixDependencies(pluginList); err != nil {
		return err
	}

	// Initialize all the Microservices destined for the Peer
	// TODO make it atomic
	for _, id := range man.Devices() {
		dev, err := vdev.Synthesize(id, man.DeviceConfiguration(id))
		if err != nil {
			return err
		}
		peer.devices[id] = dev
	}

	for _, id := range man.Coordinators() {
		coord, err := vcoord.Synthesize(id, man.CoordinatorConfiguration(id))
		if err != nil {
			return err
		}
		peer.coordinators[id] = coord
	}

	for _, id := range man.Processors() {
		proc, err := vproc.Synthesize(id, man.ProcessorConfiguration(id))
		if err != nil {
			return err
		}
		peer.processors[id] = proc
	}

	peer.logger.Printf("Peer successfully kickstarted Microservices of manifest %s", man.ID())
	return nil
}

// Devices return the instantiated Device Microservices
func (peer *Peer) Devices() map[string]device.Device {
	return peer.devices
}

// Coordinators return the instantiated Coordinator Microservices
func (peer *Peer) Coordinators() map[string]coordinator.Coordinator {
	return peer.coordinators
}

// Processors return the instantiated Processor Microservices
func (peer *Peer) Processors() map[string]processor.Processor {
	return peer.processors
}
