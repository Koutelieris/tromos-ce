package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

type DefaultDevice struct{}

func (DefaultDevice) SetBackend(device.Device) {
	panic(device.ErrBackend)
}

func (DefaultDevice) String() string {
	return "DefaultDevice"
}

func (DefaultDevice) Capabilities() []selector.Capability {
	panic(device.ErrBackend)
}

func (DefaultDevice) Location() string {
	return "localhost"
}

func (DefaultDevice) NewWriteChannel(collectionName string) (device.WriteChannel, error) {
	panic(device.ErrBackend)
}

func (DefaultDevice) NewReadChannel(string) (device.ReadChannel, error) {
	panic(device.ErrBackend)
}

func (DefaultDevice) Scan() ([]string, []device.Item, error) {
	panic(device.ErrBackend)
}
func (DefaultDevice) Close() error {
	panic(device.ErrBackend)
}
