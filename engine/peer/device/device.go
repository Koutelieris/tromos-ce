package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

// Device inherits the properties of backend device.Device and augments with
// a unique identifier used for device selection on a Mesh of Device
type Device struct {
	device.Device
	identifier   string
	peer         string
	capabilities []selector.Capability
}

func (d *Device) String() string {
	return d.identifier
}

func (d *Device) Location() string {
	return d.peer
}

func (d *Device) Capabilities() []selector.Capability {
	return d.capabilities
}
