package cluster

import (
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"net/http"

)

const (
	WebServiceURI = "*:7777"
)

func NewWebService() {
	na := NodeAgent{}

	ops := rpc.NewWebSocketService()
	ops.AddFunction("Kickoff", na.Kickoff)
	ops.AddFunction("Terminate", na.Terminate)

	na.server = &http.Server{
		Addr:    WebServiceURI,
		Handler: ops,
	}

	go na.server.ListenAndServe()
}


	return na.server.Shutdown(context.TODO())
