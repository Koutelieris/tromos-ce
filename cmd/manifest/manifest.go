package manifest // import "gitlab.com/tromos/tromos-ce/cmd/manifest"

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
)

func init() {
	ManifestCmd.AddCommand(validateCmd)
	validateCmd.Flags().String("manifest", "", "Location to manifest")
}

var ManifestCmd = &cobra.Command{
	Use:   "manifest",
	Short: "manifest related operations",
	Long:  `Handle all the operations related to a container manifest`,
}

var validateCmd = &cobra.Command{
	Use:   "validate",
	Short: "validate the manifest",
	RunE: func(cmd *cobra.Command, args []string) error {
		flags := cmd.Flags()
		if err := viper.BindPFlag("manifest", flags.Lookup("manifest")); err != nil {
			return err
		}

		localmanifest := viper.New()

		// Load a local manifest
		localmanifest.SetConfigFile(viper.GetString("manifest"))
		if err := localmanifest.ReadInConfig(); err != nil {
			return err
		}

		man, err := manifest.NewManifest(localmanifest)
		if err != nil {
			return err
		}

		if err := man.Validate(); err != nil {
			return err
		}
		return nil
	},
}
