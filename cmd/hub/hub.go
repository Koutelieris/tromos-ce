package hub // import "gitlab.com/tromos/tromos-ce/cmd/hub"

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
)

func init() {
	HubCmd.AddCommand(dependenciesCmd)

	dependenciesCmd.Flags().String("manifest", "", "Location to manifest")
	dependenciesCmd.MarkFlagRequired("manifest")
}

var HubCmd = &cobra.Command{
	Use:   "hub",
	Short: "hub related operations",
	Long:  `Handle all the operations related to the Tromos Hub`,
}

var dependenciesCmd = &cobra.Command{
	Use:   "dependencies",
	Short: "Fix the plugin dependencies of the manifest",
	RunE: func(cmd *cobra.Command, args []string) error {
		flags := cmd.Flags()
		if err := viper.BindPFlag("manifest", flags.Lookup("manifest")); err != nil {
			return err
		}

		localmanifest := viper.New()

		// Load a local manifest
		localmanifest.SetConfigFile(viper.GetString("manifest"))
		if err := localmanifest.ReadInConfig(); err != nil {
			return err
		}

		man, err := manifest.NewManifest(localmanifest)
		if err != nil {
			return err
		}

		plugins := man.PluginsOnPeer("*")

		if err := hub.FixDependencies(plugins); err != nil {
			return err
		}

		return nil
	},
}
