package scan // import "gitlab.com/tromos/tromos-ce/cmd/scan"

/*
import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"log"
)

func init() {
	cli.RootCmd.AddCommand(scanCmd)

	flags := startCmd.Flags()
	flags.String("manifest", "", "Execute from manifest")
	if err := scanCmd.MarkFlagRequired("manifest"); err != nil {
		panic(err)
	}

	flags.String("fromdevice", "", "Device to import data")
	if err := scanCmd.MarkFlagRequired("fromdevice"); err != nil {
		panic(err)
	}
}

var scanCmd = &cobra.Command{
	Use:   "scan",
	Short: "Index the contents of existing systems",
	Long:  "Index the contents of existing systems",
	RunE: func(cmd *cobra.Command, args []string) error {

		localmanifest := viper.New()
		localmanifest.SetConfigFile(viper.GetString("manifest"))
		if err := localmanifest.ReadInConfig(); err != nil {
			return err
		}

		man, err := manifest.NewManifest(localmanifest)
		if err != nil {
			return err
		}

		client, err := middleware.NewClient(middleware.ClientConfig{
			Manifest: man,
		})
		if err != nil {
			return err
		}
		defer client.Close()

		deviceid, err := cmd.Flags().GetString("fromdevice")
		if err != nil {
			log.Fatal(err)
		}

		if err := client.ImportFrom(deviceid); err != nil {
			log.Fatal(err)
		}
	},
}
*/
