package main // import "gitlab.com/tromos/tromos-ce/cmd"

import (
	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/tromos/tromos-ce/cmd/gateway"
	"gitlab.com/tromos/tromos-ce/cmd/hub"
	"gitlab.com/tromos/tromos-ce/cmd/manifest"
	"os"
	"os/signal"
)

func init() {
	// initial log formatting; this setting is updated after the daemon configuration is loaded.
	logrus.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: jsonmessage.RFC3339NanoFixed,
		FullTimestamp:   true,
	})

	RootCmd.AddCommand(gateway.GatewayCmd)
	RootCmd.AddCommand(manifest.ManifestCmd)
	RootCmd.AddCommand(hub.HubCmd)
}

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:                   "tromos",
	Short:                 "Tromos command line tool",
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func main() {
	errChan := make(chan error, 1)
	go func() {
		defer close(errChan)
		if err := RootCmd.Execute(); err != nil {
			errChan <- err
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block waiting either an error to occur to a termination signal to come
	select {
	case <-c:
		signal.Reset(os.Interrupt)
	case err := <-errChan:
		if err != nil {
			logrus.Fatal(err)
		}
	}
}
