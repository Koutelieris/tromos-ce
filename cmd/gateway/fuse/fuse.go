package fuse // import "gitlab.com/tromos/tromos-ce/cmd/gateway/fuse"

import (
	"bazil.org/fuse"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	tromosfs "gitlab.com/tromos/tromos-ce/gateway/fuse"
)

func init() {
	flags := FuseCmd.Flags()
	flags.String("mountpoint", "", "Where fuse will be mounted")
	flags.String("manifest", "", "Location to the manifest")

	FuseCmd.MarkFlagRequired("mountpoint")
	FuseCmd.MarkFlagRequired("manifest")

	if err := viper.BindPFlag("mountpoint", flags.Lookup("mountpoint")); err != nil {
		panic(err)
	}

	if err := viper.BindPFlag("manifest", flags.Lookup("manifest")); err != nil {
		panic(err)
	}

}

var FuseCmd = &cobra.Command{
	Use:           "fuse [OPTIONS]",
	Short:         "Mount a storage container as a normal filesystem",
	SilenceUsage:  true,
	SilenceErrors: true,
	//DisableFlagsInUseLine: true,
	//Args:                  cli.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {

		source := viper.GetViper()
		source.SetConfigFile(viper.GetString("manifest"))
		if err := source.ReadInConfig(); err != nil {
			return err
		}

		// Prepare the peer to the virtual infrastructure
		man, err := manifest.NewManifest(source)
		if err != nil {
			return err
		}

		// Mount the gateway
		conn, err := fuse.Mount(
			viper.GetString("mountpoint"),
			fuse.FSName("tromosfs"),
			fuse.Subtype("tromosfs"),
			fuse.AsyncRead(),
			//fuse.MaxReadahead(0xffffff),
			//fuse.WritebackCache(),
		)
		if err != nil {
			return err
		}
		defer func() {
			conn.Close()
			fuse.Unmount(viper.GetString("mountpoint"))
		}()

		client, err := middleware.NewClient(&middleware.ClientConfig{
			Manifest:      man,
			LocalDatapath: true,
		})
		if err != nil {
			return err
		}
		defer client.Close()
		return tromosfs.Mount(tromosfs.FilesystemConfig{
			Conn:   conn,
			Client: client,
		})
	},
}
