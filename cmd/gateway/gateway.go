package gateway // import "gitlab.com/tromos/tromos-ce/cmd/gateway"

import (
	"github.com/spf13/cobra"
	"gitlab.com/tromos/tromos-ce/cmd/gateway/fuse"
)

func init() {
	GatewayCmd.AddCommand(fuse.FuseCmd)
}

var GatewayCmd = &cobra.Command{
	Use:   "gateway",
	Short: "gateway related operations",
	//SilenceUsage:          true,
	//DisableFlagsInUseLine: true,
}
