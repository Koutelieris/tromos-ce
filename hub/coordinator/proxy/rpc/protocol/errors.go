package protocol

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"log"
)

// errors is a interface, thus its scope is only local.
// to be able to propagate error we use masked error codes instead
type ErrCode int

const (
	CodeOK         ErrCode = 0
	CodeProxy              = 1
	CodeErrBackend         = 2
	CodeErrNoImpl          = 3
	CodeErrKey             = 4
	CodeErrNoExist         = 5
)

func MaskError(err error) ErrCode {
	switch err {
	case nil:
		return CodeOK

	case coordinator.ErrBackend:
		return CodeErrBackend

	case coordinator.ErrNoImpl:
		return CodeErrNoImpl

	case coordinator.ErrKey:
		return CodeErrKey

	case coordinator.ErrNoExist:
		return CodeErrNoExist

	default:
		log.Printf("Uncaptured error %v", err)
		return CodeProxy
	}
}

func UnmaskError(code ErrCode) error {
	switch code {
	case CodeOK:
		return nil

	case CodeErrBackend:
		return coordinator.ErrBackend

	case CodeErrNoImpl:
		return coordinator.ErrNoImpl

	case CodeErrKey:
		return coordinator.ErrKey

	case CodeErrNoExist:
		return coordinator.ErrNoExist

	default:
		return coordinator.ErrProxy
	}
}
