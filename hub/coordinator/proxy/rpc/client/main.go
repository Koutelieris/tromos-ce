package main

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/coordinator/proxy/rpc/client/lib"
)

var Plugin coordinator.CoordinatorPlugin = proxy.New

// It is here just to keep goreleaser happy
func main() {}
