package proxy

import (
	"context"
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/coordinator/proxy/rpc/protocol"
	"net/http"
)

func New(conf *viper.Viper) coordinator.Coordinator {

	host := conf.GetString("host")
	if host == "" {
		panic("host is not defined")
	}

	port := conf.GetString("port")
	if port == "" {
		panic("port is not defined")
	}

	return &Server{
		host: host,
		port: port,
	}
}

type Server struct {
	coordinator.Coordinator
	host       string
	port       string
	webservice *http.Server
}

func (srv *Server) SetBackend(backend coordinator.Coordinator) {
	srv.Coordinator = backend

	// Lazy initialization - if there is no backend, there is no point in
	// running a server
	ops := rpc.NewWebSocketService()
	ops.AddFunction("ProxyString", backend.String)
	ops.AddFunction("ProxyCapabilities", backend.Capabilities)
	ops.AddFunction("ProxyCreateOrReset", srv.ProxyCreateOrReset)
	ops.AddFunction("ProxyInfo", srv.ProxyInfo)
	ops.AddFunction("ProxyCreateIfNotExist", srv.ProxyCreateIfNotExist)
	ops.AddFunction("ProxySetLandmark", srv.ProxySetLandmark)
	ops.AddFunction("ProxyUpdateStart", srv.ProxyUpdateStart)
	ops.AddFunction("ProxyUpdateEnd", srv.ProxyUpdateEnd)
	ops.AddFunction("ProxyViewStart", srv.ProxyViewStart)
	ops.AddFunction("ProxyViewEnd", srv.ProxyViewEnd)
	ops.AddFunction("ProxyClose", srv.Close)

	webservice := &http.Server{
		Addr:    srv.host + ":" + srv.port,
		Handler: ops,
	}
	srv.webservice = webservice

	go func() {
		if err := webservice.ListenAndServe(); err != nil {
			panic(err)
		}
	}()
}

func (srv *Server) Close() error {
	if err := srv.webservice.Shutdown(context.TODO()); err != nil {
		return err
	}
	return srv.Coordinator.Close()
}

func (srv *Server) ProxyInfo(key string) ([][]byte, map[string]string, protocol.ErrCode) {
	history, info, err := srv.Coordinator.Info(key)
	return history, info, protocol.MaskError(err)
}

func (srv *Server) ProxyCreateOrReset(key string) protocol.ErrCode {
	err := srv.Coordinator.CreateOrReset(key)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyCreateIfNotExist(key string) protocol.ErrCode {
	err := srv.Coordinator.CreateIfNotExist(key)
	return protocol.MaskError(err)
}

func (srv *Server) ProxySetLandmark(key string, mark coordinator.Landmark) protocol.ErrCode {
	err := srv.Coordinator.SetLandmark(key, mark)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyUpdateStart(key string, tid string, ir coordinator.IntentionRecord) protocol.ErrCode {
	err := srv.Coordinator.UpdateStart(key, tid, ir)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyUpdateEnd(key string, tid string, ur []byte) protocol.ErrCode {
	err := srv.Coordinator.UpdateEnd(key, tid, ur)
	return protocol.MaskError(err)
}

func (srv *Server) ProxyViewStart(key string, filter []string) ([][]byte, []string, protocol.ErrCode) {
	records, tids, err := srv.Coordinator.ViewStart(key, filter)
	return records, tids, protocol.MaskError(err)
}

func (srv *Server) ProxyViewEnd(history []string) protocol.ErrCode {
	err := srv.Coordinator.ViewEnd(history)
	return protocol.MaskError(err)
}
