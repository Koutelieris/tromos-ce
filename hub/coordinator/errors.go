package coordinator // import "gitlab.com/tromos/tromos-ce/hub/coordinator"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrCoordinator = errors.NewClass("Coordinator Error")
	ErrArg         = ErrCoordinator.NewClass("Argument error")
	ErrRuntime     = ErrCoordinator.NewClass("Runtime error")

	// Generic family errors
	ErrBackend = ErrCoordinator.New("Backend error")
	ErrNoImpl  = ErrCoordinator.New("Function not implemented")
	ErrProxy   = ErrCoordinator.New("Uncaptured PROXY ERROR")

	// Argument family errors
	ErrInvalid    = ErrArg.New("Invalid argument")
	ErrKey        = ErrArg.New("Key error")
	ErrCapability = ErrArg.New("Invalid Capability")

	// Runtime family errors
	ErrNoExist = ErrRuntime.New("Key does not exist")
)
