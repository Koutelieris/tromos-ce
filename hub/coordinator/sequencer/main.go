package main

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/coordinator/sequencer/lib"
)

var Plugin coordinator.CoordinatorPlugin = sequencer.New

// It is here just to keep goreleaser happy
func main() {}
