package sequencer

import (
	"github.com/orcaman/concurrent-map"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	"sync"
)

// operation monitor for a single key
type keymonitor struct {
	locker  sync.Mutex
	signals map[string]chan struct{}
}

func addSignal(tid string) cmap.UpsertCb {
	cb := func(exist bool, valueInMap interface{}, newValue interface{}) interface{} {
		var monitor *keymonitor
		if !exist {
			monitor = &keymonitor{signals: make(map[string]chan struct{})}
		} else {
			monitor = valueInMap.(*keymonitor)
		}

		monitor.locker.Lock()
		monitor.signals[tid] = newValue.(chan struct{})
		monitor.locker.Unlock()
		return monitor
	}
	return cb
}

func closeSignal(tid string) cmap.RemoveCb {
	cb := func(key string, v interface{}, exists bool) bool {
		if !exists {
			panic("no key monitor")
		}
		monitor := v.(*keymonitor)

		signal, ok := monitor.signals[tid]
		if !ok {
			return false
		}

		monitor.locker.Lock()
		defer monitor.locker.Unlock()

		close(signal)
		delete(monitor.signals, tid)
		// if returns true the element will be removed frm the map
		// We do not want that, as it is a slice, thus we return false
		if len(monitor.signals) == 0 {
			return true
		} else {
			return false
		}
	}
	return cb
}

func waitAllSignals(monitor *keymonitor) {
	// Use snapshot of signals to avoid rance condition with the signal
	// closing and removal.
	monitor.locker.Lock()
	var snapshot []chan struct{}
	for _, signal := range monitor.signals {
		snapshot = append(snapshot, signal)
	}
	monitor.locker.Unlock()
	for _, signal := range snapshot {
		<-signal
	}
}

func New(conf *viper.Viper) coordinator.Coordinator {

	if !conf.GetBool("blockw2r") && !conf.GetBool("blockw2w") {
		panic("At least blockw2r or blockw2w must be set")
	}

	return &Sequencer{
		w2r:    conf.GetBool("blockw2r"),
		w2w:    conf.GetBool("blockw2w"),
		writes: cmap.New(),
		reads:  cmap.New(),
	}
}

type Sequencer struct {
	coordinator.Coordinator
	w2r    bool // Block write-to-read
	w2w    bool // Block write-to-write
	writes cmap.ConcurrentMap
	reads  cmap.ConcurrentMap
}

func (seq *Sequencer) SetBackend(backend coordinator.Coordinator) {
	if backend == nil {
		panic("empty backend not allowed")
	}
	seq.Coordinator = backend
}

func (seq *Sequencer) String() string {
	return "seq <- " + seq.Coordinator.String()
}

func (seq *Sequencer) Close() error {
	if seq.writes.Count() > 0 {
		return coordinator.ErrRuntime.New("Unclosed transactions :s")
	}

	if seq.reads.Count() > 0 {
		return coordinator.ErrRuntime.New("Unclosed transactions :s")
	}
	return seq.Coordinator.Close()
}

func (seq *Sequencer) Info(key string) ([][]byte, coordinator.Info, error) {

	if seq.w2r {
		imonitor, ok := seq.writes.Get(key)
		if ok {
			waitAllSignals(imonitor.(*keymonitor))
		}
	}
	return seq.Coordinator.Info(key)
}

func (seq *Sequencer) CreateOrReset(key string) error {

	// The setlandmark operation is protected from lockers to avoid other
	// read operations reaching the data (otherwise will read stall data)
	once := uuid.Once()
	seq.writes.Upsert(key, make(chan struct{}), addSignal(once))
	defer seq.writes.RemoveCb(key, closeSignal(once))

	return seq.Coordinator.CreateOrReset(key)
}

func (seq *Sequencer) CreateIfNotExist(key string) error {

	// The setlandmark operation is protected from lockers to avoid other
	// read operations reaching the data (otherwise will read stall data)
	once := uuid.Once()
	seq.writes.Upsert(key, make(chan struct{}), addSignal(once))
	defer seq.writes.RemoveCb(key, closeSignal(once))

	return seq.Coordinator.CreateIfNotExist(key)
}

func (seq *Sequencer) SetLandmark(key string, mark coordinator.Landmark) error {

	// The setlandmark operation is protected from lockers to avoid other
	// read operations reaching the data (otherwise will read stall data)
	once := uuid.Once()
	seq.writes.Upsert(key, make(chan struct{}), addSignal(once))
	defer seq.writes.RemoveCb(key, closeSignal(once))

	return seq.Coordinator.SetLandmark(key, mark)
}

// Persist the data groups for a transaction (TID) of a key
func (seq *Sequencer) UpdateStart(key string, tid string, ir coordinator.IntentionRecord) error {

	if seq.w2w {
		imonitor, ok := seq.writes.Get(key)
		if ok {
			waitAllSignals(imonitor.(*keymonitor))
		}
	}

	seq.writes.Upsert(key, make(chan struct{}), addSignal(tid))
	return seq.Coordinator.UpdateStart(key, tid, ir)
}

func (seq *Sequencer) UpdateEnd(key string, tid string, ur []byte) error {

	// The order should be operation, singla close. Otherwise  ViewStart or Info requests
	// may sneak in. (there are free to go since the signals will be released).
	// This case is more prominent when additional layers (e.g., monitor) interleave
	// the boltdb and the seq layer.
	//
	// Although this approach protects the system from interleaving read requests it is
	// vulnerable to write-to-write requests. But this is ok since write-to-write requests
	// are serialized in the persistent coordinator layer
	defer seq.writes.RemoveCb(key, closeSignal(tid))
	return seq.Coordinator.UpdateEnd(key, tid, ur)
}

func (seq *Sequencer) ViewStart(key string, filter []string) ([][]byte, []string, error) {

	if seq.w2r {
		imonitor, ok := seq.writes.Get(key)
		if ok {
			waitAllSignals(imonitor.(*keymonitor))
		}
	}
	return seq.Coordinator.ViewStart(key, filter)
}

func (seq *Sequencer) ViewEnd(history []string) error {

	return seq.Coordinator.ViewEnd(history)
}
