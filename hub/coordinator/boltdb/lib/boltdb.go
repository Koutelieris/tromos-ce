package boltdb

import (
	"encoding/json"
	"github.com/boltdb/bolt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/path"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"os"
	"path/filepath"
)

var (
	PENDING   = []byte("PENDING")   // Records for active write transactions
	PERSISTED = []byte("PERSISTED") // Records for persisted transactions
	GARBAGES  = []byte("GARBAGES")  // Free to be removed
)

func New(conf *viper.Viper) coordinator.Coordinator {
	dbpath := conf.GetString("path")
	if dbpath == "" {
		panic("path not defined")
	}

	if conf.GetBool("cleanstart") {
		// Do not care for  since the coordinator may not exist
		os.Remove(dbpath)
	}

	// Ensure there is the parent directory
	if err := path.EnsurePath(filepath.Dir(dbpath)); err != nil {
		panic(err)
	}

	db, err := bolt.Open(dbpath, 0600, nil)
	if err != nil {
		panic(err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		_, err = tx.CreateBucketIfNotExists(GARBAGES)
		return err
	})
	if err != nil {
		panic(err)
	}
	return &BoltDB{
		db: db,
	}
}

type BoltDB struct {
	coordinator.Coordinator
	db *bolt.DB
}

func (*BoltDB) String() string {
	return "boltdb"
}

func (*BoltDB) Capabilities() []selector.Capability {
	return nil
}

func (boltdb *BoltDB) SetBackend(backend coordinator.Coordinator) {
	boltdb.Coordinator = backend
}

func (boltdb *BoltDB) Close() error {
	return boltdb.db.Close()
}

// Return all the information for the particular coordinator. Similar to
// GetStart, but without being locked waiting for other transactions
func (boltdb *BoltDB) Info(key string) ([][]byte, coordinator.Info, error) {
	records, _, err := boltdb.history(key, nil)
	if err != nil {
		return nil, nil, err
	}

	return records, coordinator.Info{}, nil
}

func (boltdb *BoltDB) CreateOrReset(key string) error {
	if len(key) == 0 {
		return coordinator.ErrKey
	}

	mark := coordinator.Landmark{IgnorePrevious: true}
	markBin, err := json.Marshal(mark)
	if err != nil {
		return err
	}
	packed, err := json.Marshal(struct {
		IsLandmark bool   `json:"IsLandmark"`
		Payload    []byte `json:"Payload"`
	}{
		true,
		markBin,
	})
	if err != nil {
		return err
	}

	return boltdb.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(key))
		if b == nil {
			b, err := tx.CreateBucket([]byte(key))
			if err != nil {
				return err
			}

			_, err = b.CreateBucket(PENDING)
			if err != nil {
				return err
			}
			_, err = b.CreateBucket(PERSISTED)
			if err != nil {
				return err
			}
			return nil
		} else {
			persisted := b.Bucket(PERSISTED)
			nextSeq, _ := persisted.NextSequence()
			return persisted.Put(structures.Itob(nextSeq), packed)
		}
	})
}

func (boltdb *BoltDB) CreateIfNotExist(key string) error {
	return boltdb.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucket([]byte(key))
		if err != nil {
			// Do nothing if the bucket already exists
			return nil
		}
		_, err = b.CreateBucket(PENDING)
		if err != nil {
			return err
		}
		_, err = b.CreateBucket(PERSISTED)
		return err
	})
}

func (boltdb *BoltDB) SetLandmark(key string, mark coordinator.Landmark) error {
	markBin, err := json.Marshal(mark)
	if err != nil {
		return err
	}
	packed, err := json.Marshal(struct {
		IsLandmark bool   `json:"IsLandmark"`
		Payload    []byte `json:"Payload"`
	}{
		true,
		markBin,
	})
	if err != nil {
		return err
	}
	// For persisted history the order is significat. Thus we use sequential
	// number for the keys. TID of the transactions is in the payload
	err = boltdb.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(key))
		if b == nil {
			return coordinator.ErrNoExist
		}
		persisted := b.Bucket(PERSISTED)
		nextSeq, _ := persisted.NextSequence()
		return persisted.Put(structures.Itob(nextSeq), packed)
	})
	return err
}

// Persist the data groups for a transaction (TID) of a key
func (boltdb *BoltDB) UpdateStart(key string, tid string, ir coordinator.IntentionRecord) error {
	// For pending  transactions order does not matter.
	// Abusively we use TID as the key, in accordance to the pending lookup
	return boltdb.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(key))
		if b == nil {
			return coordinator.ErrNoExist
		}
		pool := b.Bucket(PENDING)
		return pool.Put([]byte(tid), ir)
	})
}

func (boltdb *BoltDB) UpdateEnd(key string, tid string, ur []byte) error {
	packed, err := json.Marshal(struct {
		TID        string `json:"TID"`
		IsLandmark bool   `json:"IsLandmark"`
		Payload    []byte `json:"Payload"`
	}{
		tid,
		false,
		ur,
	})
	if err != nil {
		return err
	}
	// Remove the transaction id (TID) from pending (unordered) bucket
	// to persisted (ordered) bucket.
	err = boltdb.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(key))

		persisted := b.Bucket(PERSISTED)
		nextSeq, _ := persisted.NextSequence()
		if err := persisted.Put(structures.Itob(nextSeq), packed); err != nil {
			return err
		}

		pending := b.Bucket(PENDING)
		return pending.Delete([]byte(tid))
	})
	if err != nil {
		return err
	}
	return nil
}

// Return a list of changelogs. The changelogs are locked to to omit them from
// garbage collection. It is responsibility of the caller to free them.
//
// When filter is used, only the included tid will be returned in the corresponding
// changelogs. This is to avoid locking masked tids that are not needed
func (boltdb *BoltDB) ViewStart(key string, filter []string) ([][]byte, []string, error) {
	return boltdb.history(key, filter)
}

func (boltdb *BoltDB) ViewEnd(tids []string) error {
	return nil
}

func (boltdb *BoltDB) history(key string, filter []string) ([][]byte, []string, error) {

	var records [][]byte
	var tids []string
	err := boltdb.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(key))
		if b == nil {
			return coordinator.ErrNoExist
		}

		persisted := b.Bucket(PERSISTED)
		c := persisted.Cursor()
		for seq, packed := c.Last(); seq != nil; seq, packed = c.Prev() {

			var isLandmark bool
			var payload []byte
			var tid string
			err := json.Unmarshal(packed, &struct {
				TID        *string `json:"TID"`
				IsLandmark *bool   `json:"IsLandmark"`
				Payload    *[]byte `json:"Payload"`
			}{
				&tid,
				&isLandmark,
				&payload,
			})
			if err != nil {
				return err
			}

			switch isLandmark {
			case true:
				var mark coordinator.Landmark
				err := json.Unmarshal(payload, &mark)
				if err != nil {
					return err
				}
				if mark.IgnorePrevious {
					return nil
				}
				if mark.Disappear {
					return coordinator.ErrNoExist
				}

			case false:

				switch {
				// Ignore empty record that have no data. It can be cause by
				// Open-Close, without involved writes
				case len(payload) == 0:
					// TODO: Add it for garbage collection
					continue
				// If there is no filter, the default behavior is to include the changelog
				case len(filter) == 0:
					records = append(records, payload)
					tids = append(tids, tid)
				// If there is filter, include changelog only if tid is in the filter
				case structures.StringInSlice(tid, filter):
					records = append(records, payload)
					tids = append(tids, tid)
				}
			}
		}
		return nil
	})
	return records, tids, err
}
