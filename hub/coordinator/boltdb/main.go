package main

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/coordinator/boltdb/lib"
)

var Plugin coordinator.CoordinatorPlugin = boltdb.New

// It is here just to keep goreleaser happy
func main() {}
