package visualizer

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
	"sync"
	//"gonum.org/v1/plot/vg/draw"
)

type epoch2delay struct {
	locker sync.Mutex
	plot   *plot.Plot
	writes plotter.XYs
	reads  plotter.XYs
}

func (p *epoch2delay) init() {
	// Create a new plot, set its title and
	// axis labels.
	plot, err := plot.New()
	if err != nil {
		panic(err)
	}
	plot.Title.Text = "Requests"
	plot.X.Label.Text = "Request Epoch"
	plot.Y.Label.Text = "Delay (seconds)"
	// Draw a grid behind the data
	plot.Add(plotter.NewGrid())

	// Create placeholder for write points
	p.writes = plotter.XYs{}
	// Create placeholder for read points
	p.reads = plotter.XYs{}

	p.plot = plot
}

func (p *epoch2delay) addWriteRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	p.writes = append(p.writes,
		struct{ X, Y float64 }{float64(req.issue), float64(req.end - req.issue)},
	)
}

func (p *epoch2delay) addReadRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	p.reads = append(p.reads,
		struct{ X, Y float64 }{float64(req.issue), float64(req.end - req.issue)},
	)
}

func (p *epoch2delay) save(imagepath string) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Write Scatter
	ws, err := plotter.NewScatter(p.writes)
	if err != nil {
		panic(err)
	}
	ws.Color = plotutil.Color(2)
	ws.Shape = plotutil.Shape(6)

	// Read Scatter
	rs, err := plotter.NewScatter(p.reads)
	if err != nil {
		panic(err)
	}
	rs.Color = plotutil.Color(3)
	rs.Shape = plotutil.Shape(7)

	p.plot.Add(ws, rs)

	// Save the plot to a PDF file.
	err = p.plot.Save(4*vg.Inch, 4*vg.Inch, imagepath+"epoch2delay.pdf")
	if err != nil {
		panic(err)
	}
}
