package visualizer

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
	"sync"
	//"gonum.org/v1/plot/vg/draw"
)

type epoch2address struct {
	locker sync.Mutex
	plot   *plot.Plot
}

func (p *epoch2address) init() {
	// Create a new plot, set its title and
	// axis labels.
	plot, err := plot.New()
	if err != nil {
		panic(err)
	}
	plot.Title.Text = "Requests"
	plot.X.Label.Text = "Request Epoch"
	plot.Y.Label.Text = "Affected Address Range (bytes)"
	// Draw a grid behind the data
	plot.Add(plotter.NewGrid())

	p.plot = plot
}

func (p *epoch2address) addWriteRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Do not use range as it will make a copy
	for i := 0; i < len(req.UpdateRecord.Deltas); i++ {
		delta := &req.UpdateRecord.Deltas[i]

		points := plotter.XYs{
			{X: float64(req.issue), Y: float64(delta.Offset)},
			{X: float64(req.begin), Y: float64(delta.Offset)},
			{X: float64(req.end), Y: float64(delta.Offset + delta.Size)},
		}

		// Make a line plotter with points and set its style.
		wrLine, wrPoints, err := plotter.NewLinePoints(points)
		if err != nil {
			panic(err)
		}

		// Optional beautify
		useColor := plotutil.Color(2)

		wrLine.LineStyle.Width = vg.Points(1)
		wrLine.LineStyle.Dashes = []vg.Length{vg.Points(5), vg.Points(5)}
		wrLine.Color = useColor

		wrPoints.Shape = plotutil.Shape(6)
		wrPoints.Color = useColor

		// Register the line into the plot
		p.plot.Add(wrLine, wrPoints)
	}
}

func (p *epoch2address) addReadRequest(req *req) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Do not use range as it will make a copy
	for i := 0; i < len(req.UpdateRecord.Deltas); i++ {
		delta := &req.UpdateRecord.Deltas[i]

		points := plotter.XYs{
			{X: float64(req.issue), Y: float64(delta.Offset)},
			{X: float64(req.begin), Y: float64(delta.Offset)},
			{X: float64(req.end), Y: float64(delta.Offset + delta.Size)},
		}

		// Make a line plotter with points and set its style.
		rdLine, rdPoints, err := plotter.NewLinePoints(points)
		if err != nil {
			panic(err)
		}
		// Optional beautify
		useColor := plotutil.Color(3)

		rdLine.LineStyle.Width = vg.Points(1)
		rdLine.LineStyle.Dashes = []vg.Length{vg.Points(5), vg.Points(5)}
		rdLine.Color = useColor

		rdPoints.Shape = plotutil.Shape(7)
		rdPoints.Color = useColor

		// Register the line into the plot
		p.plot.Add(rdLine, rdPoints)
	}
}

func (p *epoch2address) save(imagepath string) {
	p.locker.Lock()
	defer p.locker.Unlock()

	// Save the plot to a PDF file.
	err := p.plot.Save(4*vg.Inch, 4*vg.Inch, imagepath+"epoch2address.pdf")
	if err != nil {
		panic(err)
	}
}
