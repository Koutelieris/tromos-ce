package visualizer

import (
	"encoding/json"
	"github.com/orcaman/concurrent-map"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"time"
)

type req struct {
	coordinator.UpdateRecord
	// Results of time.Now().Unix()
	issue int64
	begin int64
	end   int64
}

func New(conf *viper.Viper) coordinator.Coordinator {

	imagepath := conf.GetString("imagepath")
	if imagepath == "" {
		panic("invalid visualizer output path for ranges")
	}

	address := &epoch2address{}
	address.init()

	delay := &epoch2delay{}
	delay.init()

	return &Visualizer{
		activeTx:  cmap.New(),
		imagepath: imagepath,
		address:   address,
		delay:     delay,
	}
}

type Visualizer struct {
	coordinator.Coordinator
	activeTx  cmap.ConcurrentMap
	imagepath string
	address   *epoch2address
	delay     *epoch2delay
}

func (v *Visualizer) SetBackend(backend coordinator.Coordinator) {
	if backend == nil {
		panic("empty backend not allowed")
	}
	v.Coordinator = backend
}

func (v *Visualizer) String() string {
	return "visualizer <- " + v.Coordinator.String()
}

func (v *Visualizer) Close() error {
	if err := v.Coordinator.Close(); err != nil {
		return err
	}
	v.address.save(v.imagepath)
	v.delay.save(v.imagepath)
	return nil
}

func (v *Visualizer) Info(key string) ([][]byte, coordinator.Info, error) {
	req := &req{}
	req.issue = time.Now().Unix()

	history, info, err := v.Coordinator.Info(key)
	if err != nil {
		return history, info, err
	}

	req.begin = time.Now().Unix()
	req.end = req.begin

	for i := 0; i < len(history); i++ {
		v.address.addReadRequest(req)
		v.delay.addReadRequest(req)
	}
	return history, info, err
}

// Persist the data groups for a transaction (TID) of a key
func (v *Visualizer) UpdateStart(key string, tid string, ir coordinator.IntentionRecord) error {
	req := &req{}
	req.issue = time.Now().Unix()

	err := v.Coordinator.UpdateStart(key, tid, ir)
	if err != nil {
		return err
	}
	req.begin = time.Now().Unix()

	v.activeTx.Set(tid, req)
	return nil
}

func (v *Visualizer) UpdateEnd(key string, tid string, ur []byte) error {
	ireq, ok := v.activeTx.Get(tid)
	if !ok {
		return coordinator.ErrRuntime.New("UpdateStart request not found")
	}
	req := ireq.(*req)

	err := v.Coordinator.UpdateEnd(key, tid, ur)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(ur, &req.UpdateRecord); err != nil {
		panic(err)
	}

	req.end = time.Now().Unix()
	v.address.addWriteRequest(req)
	v.delay.addWriteRequest(req)
	v.activeTx.Remove(tid)
	return nil
}

func (v *Visualizer) ViewStart(key string, filter []string) ([][]byte, []string, error) {
	issue := time.Now().Unix()
	records, tids, err := v.Coordinator.ViewStart(key, filter)
	if err != nil {
		return nil, nil, err
	}

	begin := time.Now().Unix()
	for i := 0; i < len(records); i++ {

		req := &req{
			issue: issue,
			begin: begin,
		}

		if err := json.Unmarshal(records[i], &req.UpdateRecord); err != nil {
			return nil, nil, err
		}

		v.activeTx.Set(tids[i], req)
	}
	return records, tids, nil
}

func (v *Visualizer) ViewEnd(history []string) error {
	err := v.Coordinator.ViewEnd(history)
	if err != nil {
		return err
	}

	end := time.Now().Unix()
	for i := 0; i < len(history); i++ {
		ireq, ok := v.activeTx.Get(history[i])
		if !ok {
			return coordinator.ErrRuntime.New("ViewStart request not found")
		}
		req := ireq.(*req)
		req.end = end

		v.address.addReadRequest(req)
		v.delay.addReadRequest(req)
		v.activeTx.Remove(history[i])
	}
	return nil
}
