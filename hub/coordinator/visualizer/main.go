package main

import (
	"gitlab.com/tromos/tromos-ce/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/hub/coordinator/visualizer/lib"
)

var Plugin coordinator.CoordinatorPlugin = visualizer.New

// It is here just to keep goreleaser happy
func main() {}
