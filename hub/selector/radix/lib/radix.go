package radix

import (
	"github.com/hashicorp/go-immutable-radix"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

func New(conf *viper.Viper) selector.Selector {
	return &Radix{
		entries: iradix.New().Txn(),
	}
}

type Radix struct {
	entries *iradix.Txn
}

func (s *Radix) Deterministic() bool {
	return true
}

func (s *Radix) Add(p selector.SelectorProperties) {
	s.entries.Insert([]byte(p.ID), p)
}

func (s *Radix) Commit() {
	s.entries.Commit()
}

func (s *Radix) Walk(walkfn func(k []byte, v interface{}) bool) {
	s.entries.Root().Walk(walkfn)
}

func (s *Radix) Partition(key string) (string, error) {
	k, _, ok := s.entries.Root().LongestPrefix([]byte(key))
	if !ok {
		k, _, ok := s.entries.Root().Minimum()
		if !ok {
			return "", selector.ErrNoElement
		}
		return string(k), nil
	}
	return string(k), nil
}

func (s *Radix) Select(exclude []string, _ ...selector.Capability) (string, error) {
	panic("not implemented")
}
