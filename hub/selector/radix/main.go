package main

import (
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/hub/selector/radix/lib"
)

var Plugin selector.SelectorPlugin = radix.New

// It is here just to keep goreleaser happy
func main() {}
