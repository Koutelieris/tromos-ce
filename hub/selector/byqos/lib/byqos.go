package byqos

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"sync/atomic"
)

func New(conf *viper.Viper) selector.Selector {
	return &ByQOS{
		indexes: make(map[selector.Capability][]selector.SelectorProperties),
	}
}

type ByQOS struct {
	seed    int64
	indexes map[selector.Capability][]selector.SelectorProperties
}

func (s *ByQOS) Add(p selector.SelectorProperties) {
	for _, cap := range p.Capabilities {
		available := s.indexes[cap]
		available = append(available, p)
		s.indexes[cap] = available
	}
}

func (s *ByQOS) Commit() {}

func (s *ByQOS) Walk(walkfn func(k []byte, v interface{}) bool) {
	for _, index := range s.indexes {
		for _, v := range index {
			if walkfn([]byte(v.ID), v) {
				return
			}
		}
	}
}

func (s *ByQOS) Partition(key string) (string, error) {
	panic("not implemented")
}

func (s *ByQOS) Select(exclude []string, criteria ...selector.Capability) (string, error) {

	if len(criteria) == 0 {
		criteria = append(criteria, selector.Default)
	}

	for i := 0; i < len(criteria); i++ {
		proposed, ok := s.indexes[criteria[i]]
		if !ok {
			continue
		}

		// Start with an offset so to utilize all the elements.
		seed := atomic.AddInt64(&s.seed, 1) % int64(len(proposed))
		for j := int(seed); j < len(proposed); j++ {
			id := proposed[j].ID
			if !structures.StringInSlice(id, exclude) {
				return id, nil
			}
		}
	}
	return "", selector.ErrNoElement
}
