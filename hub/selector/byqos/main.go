package main

import (
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/hub/selector/byqos/lib"
)

var Plugin selector.SelectorPlugin = byqos.New

// It is here just to keep goreleaser happy
func main() {}
