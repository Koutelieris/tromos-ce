package selector // import "gitlab.com/tromos/tromos-ce/hub/selector"

import (
	"github.com/spf13/viper"
)

type SelectorPlugin func(conf *viper.Viper) Selector

type SelectorProperties struct {
	ID           string
	Capabilities []Capability
	Peer         string
}

type Selector interface {
	// Add includes a new entity in the pool
	Add(p SelectorProperties)

	// Commit is used when all entities have been included. Select and
	// Walk can be used unly after a selector has been committed
	Commit()

	// Partition returns the authority responsible for the element
	Partition(key string) (string, error)

	// Select one of the available entities in the pool based on the
	// given constrains.
	Select(exclude []string, criteria ...Capability) (string, error)

	// Walk is used when iterating the tree. Returns if iterations should
	// be terminated
	Walk(func(k []byte, v interface{}) bool)
}
