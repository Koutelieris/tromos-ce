package selector // import "gitlab.com/tromos/tromos-ce/hub/selector"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrSelector = errors.NewClass("Selector Error")
	ErrArg      = ErrSelector.NewClass("Argument error")
	ErrRuntime  = ErrSelector.NewClass("Runtime error")

	// Argument family errors
	ErrNoCriteria = ErrArg.New("No criteria have been defined")

	// Runtime family errors
	ErrExhausted = ErrRuntime.New("Elements in the pool have been exhausted")
	ErrNoElement = ErrRuntime.New("No matching element was found")
	ErrNoInit    = ErrRuntime.New("Selector has not been initialized")
)
