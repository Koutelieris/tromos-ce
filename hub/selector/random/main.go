package main

import (
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/hub/selector/random/lib"
)

var Plugin selector.SelectorPlugin = random.New

// It is here just to keep goreleaser happy
func main() {}
