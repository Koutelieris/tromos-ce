package random

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"math/rand"
)

func New(conf *viper.Viper) selector.Selector {
	return &Random{
		flat: []selector.SelectorProperties{},
	}
}

type Random struct {
	flat []selector.SelectorProperties
}

func (s *Random) Add(p selector.SelectorProperties) {
	s.flat = append(s.flat, p)
}

func (s *Random) Commit() {
}

func (s *Random) Walk(walkfn func(k []byte, v interface{}) bool) {

	for _, v := range s.flat {
		if walkfn([]byte(v.ID), v) {
			return
		}
	}
}

func (s *Random) Partition(key string) (string, error) {
	panic("Not implemented")
}

func (s *Random) Select(exclude []string, _ ...selector.Capability) (string, error) {

	if len(s.flat) <= len(exclude) {
		return "", selector.ErrExhausted
	}

	// Continuously try until find one
	for {
		id := s.flat[rand.Int()%len(s.flat)].ID
		if !structures.StringInSlice(id, exclude) {
			return id, nil
		}
	}
}
