package consistenthash

import (
	"github.com/golang/groupcache/consistenthash"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

func New(conf *viper.Viper) selector.Selector {
	return &ConsistentHash{
		index: consistenthash.New(1, nil),
		flat:  []selector.SelectorProperties{},
	}
}

type ConsistentHash struct {
	index *consistenthash.Map
	flat  []selector.SelectorProperties
}

func (s *ConsistentHash) Add(p selector.SelectorProperties) {
	s.flat = append(s.flat, p)
	s.index.Add(p.ID)
}

func (s *ConsistentHash) Commit() {
}

func (s *ConsistentHash) Walk(walkfn func(k []byte, v interface{}) bool) {
	for _, v := range s.flat {
		if walkfn([]byte(v.ID), v) {
			return
		}
	}
}

func (s *ConsistentHash) Partition(key string) (string, error) {
	return s.index.Get(key), nil
}

func (s *ConsistentHash) Select(exclude []string, _ ...selector.Capability) (string, error) {
	panic("not implemented")
}
