package main

import (
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/hub/selector/consistenthash/lib"
)

var Plugin selector.SelectorPlugin = consistenthash.New

// It is here just to keep goreleaser happy
func main() {}
