package selector // import "gitlab.com/tromos/tromos-ce/hub/selector"

type Capability int

const (
	Default Capability = iota
	InMemory
	BurstBuffer
	Cloud
	Ratecontrol
	RemoteNode
	DataStream
	ParityStream
)

var Capabilities map[string]Capability = map[string]Capability{
	"Default":      Default,
	"InMemory":     InMemory,
	"BurstBuffer":  BurstBuffer,
	"Cloud":        Cloud,
	"Ratecontrol":  Ratecontrol,
	"RemoteNode":   RemoteNode,
	"DataStream":   DataStream,
	"ParityStream": ParityStream,
}
