package main

import (
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/hub/selector/roundrobin/lib"
)

var Plugin selector.SelectorPlugin = roundrobin.New

// It is here just to keep goreleaser happy
func main() {}
