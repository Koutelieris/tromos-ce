package roundrobin

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"sync/atomic"
)

func New(conf *viper.Viper) selector.Selector {
	return &RoundRobin{
		flat: []selector.SelectorProperties{},
	}
}

type RoundRobin struct {
	prev int64
	flat []selector.SelectorProperties
}

func (s *RoundRobin) Add(p selector.SelectorProperties) {
	s.flat = append(s.flat, p)
}

func (s *RoundRobin) Commit() {
}

func (s *RoundRobin) Walk(walkfn func(k []byte, v interface{}) bool) {
	for _, v := range s.flat {
		if walkfn([]byte(v.ID), v) {
			return
		}
	}
}

func (s *RoundRobin) Partition(key string) (string, error) {
	panic("not implemented")
}

func (s *RoundRobin) Select(exclude []string, _ ...selector.Capability) (string, error) {

	if len(s.flat) <= len(exclude) {
		return "", selector.ErrExhausted
	}

repeat:
	next := atomic.AddInt64(&s.prev, 1)
	id := s.flat[int(next)%len(s.flat)].ID
	if structures.StringInSlice(id, exclude) {
		goto repeat
	}
	return id, nil
}
