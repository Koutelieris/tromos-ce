package logical

type Segment struct {
	Index uint64
	From  int64
	To    int64
}

// Logical (files) does not contain data. It contains a description of records found in one or more physical files.
type Logical interface {
	// Add adds a new update record to the tree. Offset and size indicate the affected segment of the logical file
	Add(offset int64, size int64) uint64

	// Overlap returns the indices of deltas that overlap with the request
	// When request offset is not aligned with deltas, additional bytes
	// need to be transferred. We must calculate this overhead to the buffer
	Overlaps(from int64, to int64) (size uint64, segments []Segment)

	// Length returns the contiguous space (with holes) of records
	Length() int64
}
