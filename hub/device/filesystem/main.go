package main

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/device/filesystem/lib"
)

var Plugin device.DevicePlugin = filesystem.New

// It is here just to keep goreleaser happy
func main() {}
