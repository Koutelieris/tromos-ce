= Filesystem Plugin

== Description

Gives access to a backend filesystem

== Arguments

[cols="2,2,5a", options="header"]
|===
|Field
|Type
|Description


|Family
|String
|Declare the type of filesystem that will be used as backend

* OS: use a mounted filesystem
* Memory: use an in-memory filesystem


|Path
|String
|Root within the filesystem 

|Cleanstart
|bool
|On initialization it removes all the contents from the Path

|===

==== Example

[source, yaml]
----
Devices:
    "dev0":
        Persistent:
            plugin: "filesystem"
            family: "os"
            path: "/tmp/scratch/hdd0"
        ..... Translators and Proxies ...
----

