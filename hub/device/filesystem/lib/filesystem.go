package filesystem

import (
	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/path"
	"os"
)

func New(conf *viper.Viper) device.Device {
	var fs afero.Fs
	switch conf.GetString("family") {
	case "memory":
		fs = afero.NewMemMapFs()
	case "os":
		fs = afero.NewOsFs()
	default:
		panic("Unrecognized filesystem family (Os, Mem)")
	}

	// Container and Remove Container work with ID  (e.g full path)
	// while CreateContainer with key (e.g filename).
	pathDir := conf.GetString("path")
	if pathDir == "" {
		panic("path not defined")
	}
	rootID := pathDir + "/root"

	if conf.GetBool("cleanstart") {
		if err := fs.RemoveAll(rootID); err != nil {
			panic(err)
		}
	}

	if err := path.EnsurePath(rootID); err != nil {
		panic(err)
	}

	return &Filesystem{
		Fs:   fs,
		root: rootID,
	}
}

type Filesystem struct {
	device.Device
	conf *viper.Viper
	afero.Fs
	root string
}

func (fs *Filesystem) SetBackend(backend device.Device) {
	fs.Device = backend
}

func (fs *Filesystem) String() string {
	return "filesystem:" + fs.root
}

func (fs *Filesystem) Capabilities() []selector.Capability {
	if fs.conf.GetString("family") == "memory" {
		return append(fs.Device.Capabilities(), selector.InMemory)
	}
	return nil
}

func (fs *Filesystem) Close() error {
	return nil
}

func (fs *Filesystem) NewWriteChannel(name string) (device.WriteChannel, error) {
	return &wchannel{
		Fs:     fs.Fs,
		prefix: fs.root + "/" + name,
	}, nil
}

func (fs *Filesystem) NewReadChannel(_ string) (device.ReadChannel, error) {
	return &rchannel{Fs: fs.Fs}, nil
}

func (fs *Filesystem) Scan() ([]string, []device.Item, error) {
	files := make([]string, 0)
	items := make([]device.Item, 0)
	err := afero.Walk(fs.Fs, fs.root, func(path string, info os.FileInfo, err error) error {
		// Ignore directories, pipes, sockets, ...
		if info.Mode().IsRegular() {
			files = append(files, info.Name())

			items = append(items, device.Item{
				Size: uint64(info.Size()),
				ID:   path,
			})
		}
		return nil
	})
	return files, items, err
}
