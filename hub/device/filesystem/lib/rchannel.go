package filesystem

import (
	"github.com/spf13/afero"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
)

type rchannel struct {
	afero.Fs
}

func (ch *rchannel) Close() error {
	return nil
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	file, err := ch.Fs.Open(in.Item.ID)
	if err != nil {
		return err
	}
	/* -- Question: is it better to fetch the chunks into memory
	* or to directly access the medium ?
		if _, err := file.Seek(item.Offset, 0); err != nil {
			return err
		}

		if _, err := io.CopyN(dst, file, item.Size); err != nil {
			return err
		}
	*/
	go func() {
		defer dst.Close()
		defer file.Close()

		reader := io.NewSectionReader(file, int64(in.Item.Offset), int64(in.Item.Size))
		if _, err := io.Copy(dst, reader); err != nil {
			if err := dst.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}

		if err := file.Close(); err != nil {
			if err := dst.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}
	}()
	return nil
}
