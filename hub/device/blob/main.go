package main

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/device/blob/lib"
)

var Plugin device.DevicePlugin = blob.New

// It is here just to keep goreleaser happy
func main() {}
