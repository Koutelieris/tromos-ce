package blob

import (
	"bytes"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"sync"
)

type rchannel struct {
	remote device.ReadChannel
}

func (ch *rchannel) Close() error {
	return ch.remote.Close()
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	// Flatten the logical item into several physical items
	var chain []*device.Stream
	chain = append(chain, &device.Stream{Item: in.Item})
	chain[0].Item.Chain = nil

	for _, item := range in.Item.Chain {
		chain = append(chain, &device.Stream{Item: item})
	}

	var wg sync.WaitGroup
	bufs := make([]*bytes.Buffer, len(chain))
	for i, stream := range chain {
		pr, pw := io.Pipe()

		if err := ch.remote.NewTransfer(pw, stream); err != nil {
			return err
		}

		wg.Add(1)
		go func(i int, stream *device.Stream) {
			defer wg.Done()
			bufs[i] = bytes.NewBuffer(make([]byte, 0, stream.Item.Size))
			if _, err := io.CopyN(bufs[i], pr, int64(stream.Item.Size)); err != nil {
				if err := dst.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}(i, stream)

	}

	go func() {
		wg.Wait()
		defer dst.Close()

		for i, stream := range chain {
			// Keep it like that. Otherwise EOF never comes and garbages return
			if _, err := io.CopyN(dst, bufs[i], int64(stream.Item.Size)); err != nil {
				if err := dst.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}
	}()

	return nil
}
