package blob

import (
	"code.cloudfoundry.org/bytefmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
)

func New(conf *viper.Viper) device.Device {

	bs, err := bytefmt.ToBytes(conf.GetString("blocksize"))
	if err != nil {
		panic(err)
	}

	return &Blob{blocksize: bs}
}

type Blob struct {
	device.Device
	blocksize uint64
}

func (blob *Blob) String() string {
	return "blob <- " + blob.Device.String()
}

func (blob *Blob) Capabilities() []selector.Capability {
	return append(blob.Device.Capabilities(), selector.BurstBuffer)
}

func (blob *Blob) SetBackend(backend device.Device) {
	if backend == nil {
		panic("NIL Device not allowed")
	}
	blob.Device = backend
}

func (blob *Blob) NewWriteChannel(name string) (device.WriteChannel, error) {
	channel, err := blob.Device.NewWriteChannel(name)
	if err != nil {
		return nil, err
	}
	return &wchannel{
		remote: channel,
		buffer: make([]byte, blob.blocksize),
	}, nil
}

func (blob *Blob) NewReadChannel(name string) (device.ReadChannel, error) {
	channel, err := blob.Device.NewReadChannel(name)
	if err != nil {
		return nil, err
	}
	return &rchannel{remote: channel}, nil
}

func (blob *Blob) Close() error {
	return blob.Device.Close()
}
