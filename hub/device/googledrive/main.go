package main

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/device/googledrive/lib"
)

var Plugin device.DevicePlugin = googledrive.New

// It is here just to keep goreleaser happy
func main() {}
