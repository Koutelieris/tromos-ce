package googledrive

import (
	"context"
	"github.com/prasmussen/gdrive/drive"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/pkg/oauth"
	"golang.org/x/oauth2/google"
	v3 "google.golang.org/api/drive/v3"
	"io/ioutil"
)

func New(conf *viper.Viper) device.Device {
	// Load the http client that will be used for connecting to googledrive
	b, err := ioutil.ReadFile(conf.GetString("credentials"))
	if err != nil {
		panic(err)
	}
	config, err := google.ConfigFromJSON(b, v3.DriveScope)
	if err != nil {
		panic("Unable to parse client secret file to config:" + err.Error())
	}
	client := oauth.NewClient(context.Background(), config)

	driver, err := drive.New(client)
	if err != nil {
		panic(err)
	}

	// TODO: Add clean start
	/*
		mkdirArgs := drive.MkdirArgs {
			Out: nil,
			Name:  pathDir,
			Description: "",
		}
	*/

	return &Googledrive{driver: driver}
}

type Googledrive struct {
	device.Device
	driver *drive.Drive
}

func (gd *Googledrive) SetBackend(backend device.Device) {
	gd.Device = backend
}

func (gd *Googledrive) Capabilities() []selector.Capability {
	return []selector.Capability{selector.Cloud}
}

func (gd *Googledrive) String() string {
	return "googledrive"
}

func (gd *Googledrive) NewWriteChannel(name string) (device.WriteChannel, error) {
	return &wchannel{
		gd:     gd,
		prefix: name,
	}, nil
}

func (gd *Googledrive) NewReadChannel(_ string) (device.ReadChannel, error) {
	return &rchannel{gd: gd}, nil
}

func (gd *Googledrive) Scan() ([]string, []device.Item, error) {
	return nil, nil, device.ErrNoImpl
}

func (gd *Googledrive) Close() error {
	return nil
}
