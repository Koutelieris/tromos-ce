package googledrive

import (
	"bytes"
	"fmt"
	"github.com/miolini/datacounter"
	"github.com/prasmussen/gdrive/drive"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"io/ioutil"
	"strings"
	"sync"
)

type wchannel struct {
	gd     *Googledrive
	prefix string

	transferLocker sync.Mutex
	transfers      []*device.Stream

	closed bool
}

func (ch *wchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	for _, transfer := range ch.transfers {
		<-transfer.Complete
	}
	return nil
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, stream *device.Stream) error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	pid := len(ch.transfers)

	ch.transfers = append(ch.transfers, stream)

	go func() {
		counter := datacounter.NewReaderCounter(src)

		// STUPID $#$# DRIVER.
		// ID is parsed from the output
		// For the bytes we have an intermediate counter
		key := fmt.Sprintf("%v.%v", ch.prefix, pid)
		response := bytes.NewBufferString("")
		req := drive.UploadStreamArgs{
			Out:  response,
			In:   counter,
			Name: key,
			//Parents:     []string{args.ParentID},
			Mime:  "octet-stream",
			Share: false,
			//		ChunkSize: bufLen,
			Progress: ioutil.Discard,
		}
		if err := ch.gd.driver.UploadStream(req); err != nil {
			if err := src.CloseWithError(err); err != nil {
				panic(err)
			}
			return
		}

		lines := strings.Split(response.String(), "\n")
		id := strings.Split(lines[1], " ")[1]
		if len(id) == 0 {
			if err := src.CloseWithError(device.ErrStream); err != nil {
				panic(err)
			}
		}
		stream.Item.ID = id
		stream.Item.Size = uint64(counter.Count())

		close(stream.Complete)
	}()
	return nil
}
