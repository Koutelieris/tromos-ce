package googledrive

import (
	"bytes"
	"github.com/prasmussen/gdrive/drive"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"io/ioutil"
)

type rchannel struct {
	gd    *Googledrive
	chunk *bytes.Buffer
}

func (ch *rchannel) Close() error {
	return nil
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, in *device.Stream) error {
	if ch.chunk == nil {
		ch.chunk = bytes.NewBuffer(nil)

		// Retrieve the whole chunk (potentially blob)
		req := drive.DownloadArgs{
			Progress: ioutil.Discard,
			Out:      ch.chunk,
			Id:       in.Item.ID,
			Stdout:   true, // Write data to out (instead of file)
		}
		if err := ch.gd.driver.Download(req); err != nil {
			return err
		}
	}

	go func() {
		defer dst.Close()
		_, err := io.Copy(dst, io.NewSectionReader(bytes.NewReader(ch.chunk.Bytes()), int64(in.Item.Offset), int64(in.Item.Size)))
		if err != nil {
			panic(err)
		}

	}()
	return nil
}
