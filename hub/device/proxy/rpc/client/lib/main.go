package proxy

import (
	"github.com/hashicorp/yamux"
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/protocol"
	"net"
	"sync"
	"time"
)

type WebServiceOperations struct {
	ProxyScan            func() ([]string, []device.Item, protocol.ErrCode)
	ProxyString          func() string
	ProxyCapabilities    func() []selector.Capability
	ProxyNewWriteChannel func(name string) (chanID string, err protocol.ErrCode)
	WCH_NewTransfer      func(chanID string) (err protocol.ErrCode)
	WCH_Close            func(chanID string) ([]device.Item, protocol.ErrCode)

	ProxyNewReadChannel func(name string) (chanID string, err protocol.ErrCode)
	RCH_WriteTo         func(chanID string, item device.Item) (err protocol.ErrCode)
	RCH_Close           func(chanID string) (err protocol.ErrCode)
	Close               func() error
}

type proxyclient struct {
	*WebServiceOperations
	conn *rpc.WebSocketClient
}

var once sync.Once
var proxyClientSingleton *proxyclient

func New(conf *viper.Viper) device.Device {

	host := conf.GetString("host")
	if host == "" {
		panic("host is missing")
	}

	port := conf.GetString("port")
	if port == "" {
		panic("port is missing")
	}

	timeout, err := time.ParseDuration(conf.GetString("timeout"))
	if err != nil {
		panic(err)
	}

	return &Client{
		host:    host,
		port:    port,
		timeout: timeout,
	}
}

type Client struct {
	host    string
	port    string
	timeout time.Duration
}

func (cli *Client) remote() *proxyclient {
	once.Do(func() {
		// otherwise start a new instance
		call := &WebServiceOperations{}
		conn := rpc.NewWebSocketClient("ws://" + cli.host + ":" + cli.port + "/")
		conn.SetTimeout(cli.timeout)
		conn.UseService(call)

		proxyClientSingleton = &proxyclient{WebServiceOperations: call, conn: conn}
	})
	return proxyClientSingleton
}

func (cli *Client) SetBackend(_ device.Device) {
	// Lazy initialization of the client
}

func (cli *Client) Close() error {
	return cli.remote().Close()
}

func (cli *Client) String() string {
	return cli.remote().ProxyString()
}

func (cli *Client) Capabilities() []selector.Capability {
	return cli.remote().ProxyCapabilities()
}

func (cli *Client) Location() string {
	return cli.host
}

func (cli *Client) NewWriteChannel(name string) (device.WriteChannel, error) {
	remotecaller := cli.remote()

	chanID, errcode := remotecaller.ProxyNewWriteChannel(name)
	err := protocol.UnmaskError(errcode)
	if err != nil {
		return nil, err
	}

	// Get a TCP connection
	conn, err := net.Dial("tcp", chanID)
	if err != nil {
		return nil, err
	}

	// Setup client side of yamux
	session, err := yamux.Client(conn, yamux.DefaultConfig())
	if err != nil {
		return nil, err
	}

	return &wchannel{
		remote:  remotecaller.WebServiceOperations,
		chanID:  chanID,
		session: session,
	}, nil
}

func (cli *Client) NewReadChannel(name string) (device.ReadChannel, error) {
	remotecaller := cli.remote()

	chanID, errcode := remotecaller.ProxyNewReadChannel(name)
	err := protocol.UnmaskError(errcode)
	if err != nil {
		return nil, err
	}

	// Get a TCP connection
	conn, err := net.Dial("tcp", chanID)
	if err != nil {
		return nil, err
	}

	// Setup client side of yamux
	session, err := yamux.Client(conn, yamux.DefaultConfig())
	if err != nil {
		return nil, err
	}

	return &rchannel{
		remote:  remotecaller.WebServiceOperations,
		chanID:  chanID,
		session: session,
	}, nil
}

func (cli *Client) Scan() ([]string, []device.Item, error) {
	ids, items, errcode := cli.remote().ProxyScan()
	if err := protocol.UnmaskError(errcode); err != nil {
		return nil, nil, err
	}
	return ids, items, nil
}
