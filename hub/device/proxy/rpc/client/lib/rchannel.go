package proxy

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/protocol"
	"io"
)

type rchannel struct {
	remote *WebServiceOperations
	chanID string

	session *yamux.Session
}

func (ch *rchannel) Close() (err error) {
	errcode := ch.remote.RCH_Close(ch.chanID)
	return protocol.UnmaskError(errcode)
}

func (ch *rchannel) NewTransfer(dst *io.PipeWriter, stream *device.Stream) error {
	errcode := ch.remote.RCH_WriteTo(ch.chanID, stream.Item)
	if err := protocol.UnmaskError(errcode); err != nil {
		return err
	}

	// Open a new stream
	go func() {
		defer dst.Close()
		conn, err := ch.session.Open()
		if err != nil {
			panic(err)
		}
		defer conn.Close()

		if _, err = io.Copy(dst, conn); err != nil {
			panic(err)
		}
	}()
	return nil
}
