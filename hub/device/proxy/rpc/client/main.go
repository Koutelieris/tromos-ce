package main

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/device/proxy/rpc/client/lib"
)

var Plugin device.DevicePlugin = proxy.New

// It is here just to keep goreleaser happy
func main() {}
