package protocol

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"log"
)

// errors is a interface, thus its scope is only local.
// to be able to propagate error we use masked error codes instead
type ErrCode int

const (
	CodeOK      ErrCode = 0
	CodeProxy           = 1
	CodeBackend         = 2
	CodeNoImpl          = 3
)

func MaskError(err error) ErrCode {
	switch err {
	case nil:
		return CodeOK

	case device.ErrBackend:
		return CodeBackend

	case device.ErrNoImpl:
		return CodeNoImpl

	default:
		log.Printf("Uncaptured error %v", err)
		return CodeProxy
	}
}

func UnmaskError(code ErrCode) error {
	switch code {
	case CodeOK:
		return nil

	case CodeBackend:
		return device.ErrBackend

	case CodeNoImpl:
		return device.ErrNoImpl

	default:
		return device.ErrProxy
	}
}
