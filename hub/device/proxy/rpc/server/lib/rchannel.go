package proxy

import (
	"github.com/hashicorp/yamux"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
	"sync"
)

type rchannel struct {
	remote device.ReadChannel

	transferLocker sync.Mutex
	session        *yamux.Session

	closed bool
}

func (ch *rchannel) Close() error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}
	ch.closed = true

	return ch.remote.Close()
}

func (ch *rchannel) NewTransfer(stream *device.Stream) error {
	ch.transferLocker.Lock()
	defer ch.transferLocker.Unlock()

	if ch.closed {
		panic(device.ErrChannelClosed)
	}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pw, stream); err != nil {
		return err
	}

	go func() {
		in, err := ch.session.AcceptStream()
		if err != nil {
			panic(err)
		}

		if _, err := io.Copy(in, pr); err != nil {
			panic(err)
		}

		if err := in.Close(); err != nil {
			panic(err)
		}

	}()

	return nil
}
