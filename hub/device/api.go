package device // import "gitlab.com/tromos/tromos-ce/hub/device"

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"io"
)

type DevicePlugin func(conf *viper.Viper) Device

// Device represents an object device with asynchronous data transfers.
// It consists out of several layers with common API
type Device interface {
	// SetBackend defines the backend for the current device layer
	SetBackend(Device)
	// String returns a descriptive name for the device
	String() string
	// Capabilities returns the capabilities of the device
	Capabilities() []selector.Capability
	// Location returns the node where the device is running
	Location() string
	// NewWriteChannel opens a cross-layer channel for writing data into an
	// isolated collection on the backend. Collections are immutable
	NewWriteChannel(collectionName string) (WriteChannel, error)
	// NewReadChannels opens an cross-layer channel for reading data from an
	// isolated collection on the backend
	NewReadChannel(string) (ReadChannel, error)
	// Scan returns a list of items and metadata that exist on the backend
	// associated with the virtual device
	Scan() ([]string, []Item, error)
	// Close shutdowns the device
	Close() error
}

// WriteChannel is an entity that crosses all layers on the device and is
// terminated into a collection (e.g., directory, bucket, prefix) on the backend
type WriteChannel interface {
	// NewStream initiates an asynchronous transfer to the device
	// The error indicates a mishappen during preparation phase.
	// Completion and errors are signalled back through pipe (see PipeError)
	NewTransfer(src *io.PipeReader, stream *Stream) error
	// Close blocks until all the pending streams are flushed and terminates
	// the channel. Second call to close should cause a panic. If there are any
	// errors occurred in the streams, Close() returns ErrStream. For the exact
	// error the caller must iterate the streams and use the Complete field to
	// extract the error code
	Close() error
}

// ReadChannel is an entity that crosses all layers on the device and reads
// data from a collection on the backend.
type ReadChannel interface {
	// NewTransfer initiates an asynchronous data transfer from an Item from a
	// collection from the backend. In this mode, the completion signal of the
	// stream is not needed. Completion and errors are signalled back through
	// pipe (see PipeError)
	NewTransfer(dst *io.PipeWriter, steam *Stream) error
	// Close blocks until all read streams are finished and terminates the
	// channel. Any state is released after the channel is closed
	Close() error
}

// Stream is a descriptor for an asynchronous transfer. It is up to the client
// to populate the completion fields and the server use it to signal completion.
// For correctness the client must first wait for the completion signal
// (and check its status) and then check the metadata. Otherwise it may cause
// a race condition.
//
// In the general case, the stream metadata are stable only after the channel
// has been closed. In the meantime, the item state is in flux and should not
// be taken into account.
type Stream struct {
	Item     Item          `json:"Item"` // Transfer's metadata
	Complete chan struct{} `json:"-"`    // Transfer's status.
}

// Item describes the metadata of a logically written element. Depending on
// the Device layers, an item may consists out of several physical elements.
// In that case the Chain provides access to all of the physical elements of
// the described logical element.
type Item struct {
	ID     string `json:"ID" validate:"require,len=8,regexp=^[a-zA-Z0-9]*$"` // Element's identifier (e.g., path, id)
	Size   uint64 `json:"Size"`                                              // Element's size
	Offset uint64 `json:"Offset"`                                            // Element's offset within a blob (if any)
	Chain  []Item `json:"Chain,omitempty"`                                   // Other physical elements
}

// IsEmpty returns true if a logical item is empty
func (i *Item) IsEmpty() bool {
	if i.Size == 0 && i.ID == "" && len(i.Chain) == 0 {
		return true
	}
	return false
}
