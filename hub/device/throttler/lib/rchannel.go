package throttler

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
)

type rchannel struct {
	device.ReadChannel
}
