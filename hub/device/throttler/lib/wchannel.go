package throttler

import (
	"github.com/juju/ratelimit"
	"gitlab.com/tromos/tromos-ce/hub/device"
	"io"
)

// Allows only for one stream
type wchannel struct {
	throttle *Throttle
	bucket   *ratelimit.Bucket
	remote   device.WriteChannel
}

func (ch *wchannel) Close() error {
	return ch.remote.Close()
}

func (ch *wchannel) NewTransfer(src *io.PipeReader, stream *device.Stream) error {
	bucket := ch.bucket
	if ch.throttle.regulate == Stream {
		bucket = ratelimit.NewBucketWithRate(ch.throttle.rate, ch.throttle.capacity)
	}

	pr, pw := io.Pipe()
	if err := ch.remote.NewTransfer(pr, stream); err != nil {
		return err
	}

	go func() {
		if _, err := io.Copy(pw, ratelimit.Reader(src, bucket)); err != nil {
			panic(err)
		}
		if err := pw.Close(); err != nil {
			panic(err)
		}
	}()

	return nil
}
