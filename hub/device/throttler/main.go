package main

import (
	"gitlab.com/tromos/tromos-ce/hub/device"
	"gitlab.com/tromos/tromos-ce/hub/device/throttler/lib"
)

var Plugin device.DevicePlugin = throttler.New

// It is here just to keep goreleaser happy
func main() {}
