package device // import "gitlab.com/tromos/tromos-ce/hub/device"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrDevice  = errors.NewClass("Device Error")
	ErrArg     = ErrDevice.NewClass("Argument error")
	ErrRuntime = ErrDevice.NewClass("Runtime error")

	// Generic family errors
	ErrBackend = ErrDevice.New("Backend error")
	ErrNoImpl  = ErrDevice.New("Function not implemented")
	ErrProxy   = ErrDevice.New("Uncaptured PROXY ERROR")

	// Argument family errors
	ErrInvalid    = ErrArg.New("Invalid argument")
	ErrCapability = ErrArg.New("Invalid Capability")

	// Runtime family errors
	ErrChannelClosed = ErrRuntime.New("Action on closed channel")
	ErrStream        = ErrRuntime.New("Stream transfer error")
)
