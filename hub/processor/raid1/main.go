package main

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/raid1/lib"
)

var Plugin processor.ProcessorPlugin = raid1.New

// It is here just to keep goreleaser happy
func main() {}
