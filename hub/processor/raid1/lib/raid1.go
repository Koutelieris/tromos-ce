package raid1

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/aes"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/mirror"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sha256"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sink"
)

// Options
// Replicas: Number of replicas
// Encrypt: whether to encrypt the replicas
// IntegrityCheck: whether to implement integrirty checking
func New(config *viper.Viper) processor.ProcessGraph {
	return &graph{Viper: config}
}

type graph struct {
	*viper.Viper
}

func (*graph) Reusable() bool {
	return true
}

// Bottom-up description, with intermediate placeholders
func (graph *graph) Upstream(b processor.Builder) {

	b.Add("fw0", &sink.Uplink{})
	if graph.GetBool("integritycheck") {
		b.Add("checksum", sha256.NewUplink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "fw0", "In")
	} else {
		b.MapInPort("In", "fw0", "In")
	}

	b.Add("mirror", mirror.NewUplink())
	if graph.GetBool("encrypt") {
		b.Add("encrypter", aes.NewUplink(0,
			graph.GetString("passphrase")))
		b.Connect("fw0", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "mirror", "In")
	} else {
		b.Connect("fw0", "Out", "mirror", "In")
	}

	for i := 0; i < graph.GetInt("replicas"); i++ {
		sinkID := fmt.Sprintf("sink%v", i)
		b.Add(sinkID, &sink.Uplink{})
		b.Connect("mirror", "Out", sinkID, "In")
		b.MapOutPort(sinkID, sinkID, "Out")
	}
}

// Top-down description, without intermediate placeholders. Starting from the most
// specific to the more generic
func (graph *graph) Downstream(b processor.Builder) {

	b.Add("mirror", mirror.NewDownlink())
	switch {
	case graph.GetBool("integritycheck") && graph.GetBool("encrypt"):
		b.Add("checksum", sha256.NewDownlink())
		b.Add("encrypter", aes.NewDownlink(0, graph.GetString("passphrase")))

		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "mirror", "In")

	case graph.GetBool("integritycheck"):
		b.Add("checksum", sha256.NewDownlink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "mirror", "In")

	case graph.GetBool("encrypt"):
		b.Add("encrypter", aes.NewDownlink(0, graph.GetString("passphrase")))
		b.MapInPort("In", "encrypter", "In")
		b.Connect("encrypter", "Out", "mirror", "In")
	}

	for i := 0; i < graph.GetInt("replicas"); i++ {
		sinkID := fmt.Sprintf("sink%v", i)
		b.Add(sinkID, &sink.Uplink{})
		b.Connect("mirror", "Out", sinkID, "In")
		b.MapOutPort(sinkID, sinkID, "Out")
	}
}
