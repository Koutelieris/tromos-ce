package processor // import "gitlab.com/tromos/tromos-ce/hub/processor"

import (
	"io"
)

type ReadWriteCloser interface {
	io.ReadWriteCloser
	CloseWithError(err error) error
}

type Receiver struct {
	*io.PipeReader
}

func (r Receiver) Write(data []byte) (n int, err error) {
	panic("Write on receiver is not allowed")
}

type Sender struct {
	*io.PipeWriter
}

func (s Sender) Read(p []byte) (n int, err error) {
	panic("Read on sender is not allowed")
}

func Pipe() (ReadWriteCloser, ReadWriteCloser) {
	pr, pw := io.Pipe()
	return Receiver{pr}, Sender{pw}
}
