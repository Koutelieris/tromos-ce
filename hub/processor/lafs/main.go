package main

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/lafs/lib"
)

var Plugin processor.ProcessorPlugin = lafs.New

// It is here just to keep goreleaser happy
func main() {}
