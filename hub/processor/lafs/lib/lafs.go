package lafs

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/selector"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/aes"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/reedsolomon"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sha256"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sink"
)

func New(config *viper.Viper) processor.ProcessGraph {
	return &LAFS{Viper: config}
}

type LAFS struct {
	*viper.Viper
}

func (*LAFS) Reusable() bool {
	return true
}

func (config *LAFS) Upstream(b processor.Builder) {
	b.Add("fw0", &sink.Uplink{})
	if config.GetBool("integritycheck") {
		b.Add("checksum", sha256.NewUplink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "fw0", "In")
	} else {
		b.MapInPort("In", "fw0", "In")
	}

	b.Add("erasure", reedsolomon.NewUplink(
		config.GetInt("datablocks"),
		config.GetInt("parityblocks"),
		config.GetInt("blocksize"),
	))
	if config.GetBool("encrypt") {
		b.Add("encrypter", aes.NewUplink(0, config.GetString("passphrase")))
		b.Connect("fw0", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "erasure", "In")
	} else {
		b.Connect("fw0", "Out", "erasure", "In")
	}

	var datasinks int
	for i := 0; i < config.GetInt("datablocks"); i++ {
		sinkID := fmt.Sprintf("sink%v", i)
		b.Add(sinkID, &sink.Uplink{})

		b.Connect("erasure", "Out", sinkID, "In")
		b.MapOutPort(sinkID, sinkID, "Out", selector.DataStream)
		datasinks = i
	}

	for i := 0; i < config.GetInt("parityblocks"); i++ {
		sinkID := fmt.Sprintf("sink%v", i+datasinks+1)
		b.Add(sinkID, &sink.Uplink{})

		b.Connect("erasure", "Out", sinkID, "In")
		b.MapOutPort(sinkID, sinkID, "Out", selector.ParityStream)
	}
}

func (config *LAFS) Downstream(b processor.Builder) {
	b.Add("fw0", &sink.Downlink{})
	if config.GetBool("integritycheck") {
		b.Add("checksum", sha256.NewDownlink())
		b.MapInPort("In", "checksum", "In")
		b.Connect("checksum", "Out", "fw0", "In")
	} else {
		b.MapInPort("In", "fw0", "In")
	}

	b.Add("erasure", reedsolomon.NewDownlink(
		config.GetInt("datablocks"),
		config.GetInt("parityblocks"),
		config.GetInt("blocksize"),
	))
	if config.GetBool("encrypt") {
		b.Add("encrypter", aes.NewDownlink(0, config.GetString("passphrase")))
		b.Connect("fw0", "Out", "encrypter", "In")
		b.Connect("encrypter", "Out", "erasure", "In")
	} else {
		b.Connect("fw0", "Out", "erasure", "In")
	}

	var datasinks int
	for i := 0; i < config.GetInt("datablocks"); i++ {
		sinkID := fmt.Sprintf("sink%v", i)
		b.Add(sinkID, &sink.Downlink{})

		b.Connect("erasure", "Out", sinkID, "In")
		b.MapOutPort(sinkID, sinkID, "Out", selector.DataStream)
		datasinks = i
	}

	for i := 0; i < config.GetInt("parityblocks"); i++ {
		sinkID := fmt.Sprintf("sink%v", i+datasinks+1)
		b.Add(sinkID, &sink.Downlink{})

		b.Connect("erasure", "Out", sinkID, "In")
		b.MapOutPort(sinkID, sinkID, "Out", selector.ParityStream)
	}
}
