package pipeline

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/aes"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/mirror"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sha256"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sink"
)

func New(_ *viper.Viper) processor.ProcessGraph {
	return &Pipeline{}
}

type Pipeline struct{}

func (d *Pipeline) Reusable() bool {
	return false
}

func (d *Pipeline) Upstream(b processor.Builder) {
	b.Add("feed", &sink.Uplink{})
	b.MapInPort("In", "feed", "In")

	b.Add("checksum", sha256.NewUplink())
	b.Connect("feed", "Out", "checksum", "In")

	b.Add("mirror", mirror.NewUplink())
	b.Connect("checksum", "Out", "mirror", "In")

	b.Add("encrypt", aes.NewUplink(0, "lolabunnysixteen"))
	b.Connect("mirror", "Out", "encrypt", "In")

	b.Add("sink", &sink.Uplink{})
	b.Connect("encrypt", "Out", "sink", "In")

	b.MapOutPort("s0", "sink", "Out")

	b.Add("nilbranch", &sink.Uplink{})
	b.Connect("mirror", "Out", "nilbranch", "In")
	b.MapOutPort("s1", "nilbranch", "Out")
}

func (d *Pipeline) Downstream(b processor.Builder) {
	b.Add("feed", &sink.Downlink{})
	b.MapInPort("In", "feed", "In")

	b.Add("checksum", sha256.NewDownlink())
	b.Connect("feed", "Out", "checksum", "In")

	b.Add("mirror", mirror.NewDownlink())
	b.Connect("checksum", "Out", "mirror", "In")

	b.Add("encrypt", aes.NewDownlink(0, "lolabunnysixteen"))
	b.Connect("mirror", "Out", "encrypt", "In")

	b.Add("sink", &sink.Downlink{})
	b.Connect("encrypt", "Out", "sink", "In")

	b.MapOutPort("s0", "sink", "Out")

	b.Add("nilbranch", &sink.Downlink{})
	b.Connect("mirror", "Out", "nilbranch", "In")
	b.MapOutPort("s1", "nilbranch", "Out")
}
