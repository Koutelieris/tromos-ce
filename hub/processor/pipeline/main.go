package main

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/pipeline/lib"
)

var Plugin processor.ProcessorPlugin = pipeline.New

// It is here just to keep goreleaser happy
func main() {}
