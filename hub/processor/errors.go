package processor // import "gitlab.com/tromos/tromos-ce/hub/processor"

import (
	"github.com/spacemonkeygo/errors"
)

var (
	ErrProcessor = errors.NewClass("Processor Error")
	ErrArg       = ErrProcessor.NewClass("Argument error")
	ErrRuntime   = ErrProcessor.NewClass("Runtime error")

	// Generic family errors
	ErrNoImpl = ErrProcessor.New("Function not implemented")

	// Argument family errors
	ErrInvalid    = ErrArg.New("Invalid argument")
	ErrCapability = ErrArg.New("Invalid Capability")

	// Runtime family errors
	ErrCorrupted = ErrProcessor.New("Corrupted Data")
)
