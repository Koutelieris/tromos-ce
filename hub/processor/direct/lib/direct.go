package direct

import (
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/module/sink"
)

func New(_ *viper.Viper) processor.ProcessGraph {
	return &Direct{}
}

type Direct struct{}

func (d *Direct) Reusable() bool {
	return true
}

func (d *Direct) Upstream(b processor.Builder) {
	b.Add("sink0", &sink.Uplink{})

	b.MapInPort("In", "sink0", "In")
	b.MapOutPort("s0", "sink0", "Out")
}

func (d *Direct) Downstream(b processor.Builder) {
	b.Add("sink0", &sink.Downlink{})

	b.MapInPort("In", "sink0", "In")
	b.MapOutPort("s0", "sink0", "Out")
}
