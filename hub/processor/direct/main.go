package main

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"gitlab.com/tromos/tromos-ce/hub/processor/direct/lib"
)

var Plugin processor.ProcessorPlugin = direct.New

// It is here just to keep goreleaser happy
func main() {}
