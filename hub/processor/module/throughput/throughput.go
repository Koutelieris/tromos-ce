package throughput

import (
	"fmt"
	"github.com/aybabtme/iocontrol"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In     <-chan *processor.Stream
	Out    chan<- *processor.Stream
	writer *iocontrol.MeasuredWriter
}

func (up *Uplink) Finish() {
	if up.writer == nil {
		return
	}
	fmt.Println("Uptream bytes per sec: ", up.writer.BytesPerSec())
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.process(in)
	}
}

func (up *Uplink) process(in *processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	up.writer = iocontrol.NewMeasuredWriter(pw)

	_, err := io.Copy(up.writer, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In     <-chan *processor.Stream
	Out    chan<- *processor.Stream
	reader *iocontrol.MeasuredReader
}

func (down *Downlink) Finish() {
	if down.reader == nil {
		return
	}
	fmt.Println("Downstream bytes per sec: ", down.reader.BytesPerSec())
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.process(in)
	}
}

func (down *Downlink) process(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	down.reader = iocontrol.NewMeasuredReader(pr)

	_, err := io.Copy(in.Data, down.reader)
	if err != nil {
		panic(err)
	}
}
