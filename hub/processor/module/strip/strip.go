package strip

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In         <-chan *processor.Stream
	Out        []chan<- *processor.Stream
	stripesize int64
}

func NewUplink(stripesize int64) *Uplink {

	if stripesize == 0 {
		panic("Invalid stripe size")
	}
	up := &Uplink{stripesize: stripesize}
	return up
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.process(in)
	}
}

func (up *Uplink) process(in *processor.Stream) {
	fanout := len(up.Out)
	pr := make([]processor.ReadWriteCloser, fanout)
	pw := make([]processor.ReadWriteCloser, fanout)
	for i := 0; i < fanout; i++ {
		pr[i], pw[i] = processor.Pipe()
		defer pw[i].Close()

		up.Out[i] <- &processor.Stream{
			Data: pr[i],
			Meta: in.Meta,
		}
	}

	// Round-robin write to stripes
	var s int
	for i := 0; ; i++ {
		s = i % fanout
		_, err := io.CopyN(pw[s], in.Data, up.stripesize)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

type Downlink struct {
	processor.Module
	In         <-chan *processor.Stream
	Out        []chan<- *processor.Stream
	stripesize int64
}

func NewDownlink(stripesize int64) *Downlink {
	down := &Downlink{stripesize: stripesize}
	return down
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.process(in)
	}
}

func (down *Downlink) process(in *processor.Stream) {
	defer in.Data.Close()

	fanout := len(down.Out)
	pr := make([]processor.ReadWriteCloser, fanout)
	pw := make([]processor.ReadWriteCloser, fanout)
	for i := 0; i < fanout; i++ {
		pr[i], pw[i] = processor.Pipe()

		down.Out[i] <- &processor.Stream{
			Data: pw[i],
			Meta: in.Meta,
		}
	}

	// Round-robin read of the buffers to reconstruct the image
	// Return EOF only if data from all streams are consumed
	var s int
	isClosed := make([]bool, fanout)
	for i, done := 0, 0; done < fanout; i++ {
		s = i % fanout
		if !isClosed[s] {
			continue
		}
		_, err := io.CopyN(in.Data, pr[s], down.stripesize)
		if err == io.EOF {
			pr[s].Close()
			isClosed[s] = true
			done++
			continue
		}
		if err != nil {
			panic(err)
		}
	}
}
