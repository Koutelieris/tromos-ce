package reedsolomon

import (
	"github.com/klauspost/reedsolomon"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
	"os"
	"strconv"
)

type Uplink struct {
	processor.Module
	In           <-chan *processor.Stream
	Out          []chan<- *processor.Stream
	erasure      reedsolomon.Encoder
	dataBlocks   int
	parityBlocks int
	blockSize    int64
}

func NewUplink(dataBlocks int, parityBlocks int, blockSize int) *Uplink {

	if blockSize == 0 {
		panic(processor.ErrArg)
	}

	erasure, err := reedsolomon.New(dataBlocks, parityBlocks)
	if err != nil {
		panic(err)
	}

	return &Uplink{
		erasure:      erasure,
		dataBlocks:   dataBlocks,
		parityBlocks: parityBlocks,
		blockSize:    int64(blockSize),
	}
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.process(in)
	}
}

func (up *Uplink) process(in *processor.Stream) {
	// Validate sharding schema
	fanout := up.dataBlocks + up.parityBlocks
	if fanout != len(up.Out) {
		panic("Inconsistency between number of shards and outputs")
	}

	// Forwarders to other components
	pr := make([]processor.ReadWriteCloser, fanout)
	pw := make([]processor.ReadWriteCloser, fanout)
	for i := 0; i < fanout; i++ {
		pr[i], pw[i] = processor.Pipe()
		defer pw[i].Close()

		up.Out[i] <- &processor.Stream{
			Data: pr[i],
			Meta: in.Meta,
		}
	}

	var perShardSize int
	var total int
	block := make([]byte, up.blockSize)
	for {
		// Locally copy the data to be processored
		rb, err := io.ReadFull(in.Data, block)
		switch {
		case err == io.EOF:
			// EOF only if no bytes were read (closed stream without data)
			in.Meta.State["total"] = strconv.Itoa(total)
			in.Meta.State["shardsize"] = strconv.Itoa(perShardSize)
			return
		case err == io.ErrUnexpectedEOF || err == nil:
			// If an EOF happens after reading fewer than min bytes, ReadAtLeast returns ErrUnexpectedEOF
			// It happens when the block size is smaller than the data source
			goto process
		default:
			panic(err)
		}
	process:
		// Split the data block to multiple encoded blocks
		encoded, err := up.erasure.Split(block)
		if err != nil {
			panic(err)
		}
		perShardSize = len(encoded[0])

		// Do the encoding
		if err := up.erasure.Encode(encoded); err != nil {
			panic(err)
		}

		// Asynchronously push the encoded data to the storage pipes
		for i := 0; i < len(encoded); i++ {
			_, err := pw[i].Write(encoded[i])
			if err != nil {
				panic(err)
			}
		}
		total += rb
	}
}

type Downlink struct {
	processor.Module
	In           <-chan *processor.Stream
	Out          []chan<- *processor.Stream
	erasure      reedsolomon.Encoder
	dataBlocks   int
	parityBlocks int
	blockSize    int64
}

func NewDownlink(dataBlocks int, parityBlocks int, blockSize int) *Downlink {

	if blockSize == 0 {
		panic(processor.ErrArg)
	}

	erasure, err := reedsolomon.New(dataBlocks, parityBlocks)
	if err != nil {
		panic(err)
	}

	return &Downlink{
		erasure:      erasure,
		dataBlocks:   dataBlocks,
		parityBlocks: parityBlocks,
		blockSize:    int64(blockSize),
	}
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.process(in)
	}
}

func (down *Downlink) process(in *processor.Stream) {
	defer in.Data.Close()

	// Validate sharding schema
	fanout := down.dataBlocks + down.parityBlocks
	if fanout != len(down.Out) {
		panic("Inconsistency between number of shards and outputs")
	}

	// Load the state from the previous write. It is needed since the data
	// may be less than the blocksize and therefore padded with 0. We need
	// to ignore those 0 when return the original content
	total, err := strconv.Atoi(in.Meta.State["total"])
	if err != nil {
		panic(err)
	}

	// In the current form, the pipe acts an interleaver of data
	// of different iterations. (data[0][0], data[1][0], ..)
	//
	// To avoid keeping much state, we assume that all the iterations
	// serve exactly the same amount of data, and those data are stored
	// to shards with size perShardSize.
	perShardSize, err := strconv.Atoi(in.Meta.State["shardsize"])
	if err != nil {
		panic(err)
	}

	pr := make([]processor.ReadWriteCloser, fanout)
	pw := make([]processor.ReadWriteCloser, fanout)
	blocks := make([][]byte, fanout)
	for i := 0; i < fanout; i++ {
		pr[i], pw[i] = processor.Pipe()
		down.Out[i] <- &processor.Stream{
			Data: pw[i],
			Meta: in.Meta,
		}

		blocks[i] = make([]byte, perShardSize)
	}

	for remaining := total; remaining > 0; remaining -= int(down.blockSize) {
		for i := 0; i < fanout; i++ {
			_, err := io.ReadFull(pr[i], blocks[i])
			if os.IsNotExist(err) {
				// Indicate that a shard is missing by setting it to nil or zero-length.
				blocks[i] = blocks[i][:0]
				continue
			}

			if err == io.EOF {
				// Move to the next block. This may happen either if the current block
				// is missing (not exist) or if the current block is full and there are
				// more data to read
				continue
			}
			if err != nil {
				if err := in.Data.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}
		if err := down.erasure.ReconstructData(blocks); err != nil {
			if err := in.Data.CloseWithError(processor.ErrCorrupted); err != nil {
				panic(err)
			}
			return
		}

		// Handler for the last block
		if remaining < int(down.blockSize) {
			err := down.erasure.Join(in.Data, blocks, int(remaining))
			if err != nil {
				if err := in.Data.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		} else {
			// All intermediate iterations
			if err := down.erasure.Join(in.Data, blocks, int(down.blockSize)); err != nil {
				if err := in.Data.CloseWithError(err); err != nil {
					panic(err)
				}
				return
			}
		}
	}
}
