package sha256

import (
	"crypto/sha256"
	"fmt"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func NewUplink() *Uplink {
	return &Uplink{}
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.process(in)
	}
}

func (up *Uplink) process(in *processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	// TeeReader returns a Reader that writes to w what it reads from r
	tee := io.TeeReader(in.Data, pw)

	h := sha256.New()
	if _, err := io.Copy(h, tee); err != nil {
		if err := in.Data.CloseWithError(err); err != nil {
			panic(err)
		}
	}

	in.Meta.State["checksum"] = fmt.Sprintf("%x", h.Sum(nil))
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.process(in)
	}
}

func (down *Downlink) process(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	// TeeReader returns a Reader that writes to w what it reads from r
	tee := io.TeeReader(pr, in.Data)

	h := sha256.New()
	if _, err := io.Copy(h, tee); err != nil {
		if err := in.Data.CloseWithError(err); err != nil {
			panic(err)
		}
		return
	}

	if fmt.Sprintf("%x", h.Sum(nil)) != in.Meta.State["checksum"] {
		if err := in.Data.CloseWithError(processor.ErrCorrupted); err != nil {
			panic(err)
		}
		return
	}
}
