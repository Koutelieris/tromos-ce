package mirror

import (
	"github.com/djherbis/stream"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out []chan<- *processor.Stream
}

func NewUplink() *Uplink {
	return &Uplink{}
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.process(in)
	}
}

func (up *Uplink) process(in *processor.Stream) {

	w, err := stream.NewStream("", stream.NewMemFS())
	if err != nil {
		panic(err)
	}

	go func() {
		if _, err := io.Copy(w, in.Data); err != nil {
			panic(err)
		}
		w.Close()
	}()

	for i := 0; i < len(up.Out); i++ {
		r, err := w.NextReader()
		if err != nil {
			panic(err)
		}
		defer r.Close()

		pr, pw := processor.Pipe()
		up.Out[i] <- &processor.Stream{
			Data: pr,
			Meta: in.Meta,
		}

		if _, err := io.Copy(pw, r); err != nil {
			panic(err)
		}

		if err := pw.Close(); err != nil {
			panic(err)
		}
	}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out []chan<- *processor.Stream
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.process(in)
	}
}

func (down *Downlink) process(in *processor.Stream) {
	// Always read from the first replicate
	// FIXME: add selection algorithm (probably another module)
	down.Out[0] <- in

	for i := 1; i < len(down.Out); i++ {
		down.Out[i] <- nil
	}
}
