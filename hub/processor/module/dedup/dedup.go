package dedup

import (
	"github.com/klauspost/dedup"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.process(in)
	}
}

func (up *Uplink) process(in *processor.Stream) {
	pr, pw := processor.Pipe()

	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	// Create a new writer, with each block being 1000 bytes,
	// And allow it to use 10000 bytes of memory
	w, err := dedup.NewStreamWriter(pw, dedup.ModeFixed, 1000, 10000)
	if err != nil {
		panic(err)
	}
	defer w.Close()

	_, err = io.Copy(w, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.process(in)
	}
}

func (down *Downlink) process(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	// Create a new stream reader:
	r, err := dedup.NewStreamReader(pr)
	if err != nil {
		panic(err)
	}
	defer r.Close()

	_, err = io.Copy(in.Data, r)
	if err != nil && err != io.EOF {
		panic(err)
	}
}
