package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream

	mode int
	key  string
}

func NewUplink(mode int, key string) *Uplink {
	return &Uplink{
		mode: mode,
		key:  key,
	}
}

func (up *Uplink) OnIn(in *processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()
	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	block, err := aes.NewCipher([]byte(up.key))
	if err != nil {
		panic(err)
	}

	var iv [aes.BlockSize]byte
	stream := cipher.NewOFB(block, iv[:])
	encrypter := &cipher.StreamWriter{S: stream, W: pw}
	defer encrypter.Close()

	_, err = io.Copy(encrypter, in.Data)
	if err != nil {
		panic(err)
	}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream

	mode int
	key  string
}

func NewDownlink(mode int, key string) *Downlink {

	return &Downlink{
		mode: mode,
		key:  key,
	}
}

func (down *Downlink) OnIn(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	block, err := aes.NewCipher([]byte(down.key))
	if err != nil {
		panic(err)
	}

	var iv [aes.BlockSize]byte
	stream := cipher.NewOFB(block, iv[:])

	decrypter := &cipher.StreamReader{S: stream, R: pr}

	if _, err := io.Copy(in.Data, decrypter); err != nil {
		panic(err)
	}
}
