package zstd

import (
	"github.com/DataDog/zstd"
	"gitlab.com/tromos/tromos-ce/hub/processor"
	"io"
)

func NewUplink() *Uplink {
	return &Uplink{}
}

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.process(in)
	}
}

func (up *Uplink) process(in *processor.Stream) {
	pr, pw := processor.Pipe()
	defer pw.Close()

	up.Out <- &processor.Stream{
		Data: pr,
		Meta: in.Meta,
	}

	compressor := zstd.NewWriter(pw)
	defer compressor.Close()

	_, err := io.Copy(compressor, in.Data)
	if err != nil {
		panic(err)
	}
}

func NewDownlink() *Downlink {
	return &Downlink{}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.process(in)
	}
}

func (down *Downlink) process(in *processor.Stream) {
	defer in.Data.Close()

	pr, pw := processor.Pipe()
	down.Out <- &processor.Stream{
		Data: pw,
		Meta: in.Meta,
	}

	decompressor := zstd.NewReader(pr)
	//defer decompressor.Close()
	//
	_, err := io.Copy(in.Data, decompressor)
	if err != nil {
		panic(err)
	}
}
