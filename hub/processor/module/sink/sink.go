package sink

import (
	"gitlab.com/tromos/tromos-ce/hub/processor"
)

type Uplink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (up *Uplink) Process() {
	for in := range up.In {
		up.Out <- in

	}
}

type Downlink struct {
	processor.Module
	In  <-chan *processor.Stream
	Out chan<- *processor.Stream
}

func (down *Downlink) Process() {
	for in := range down.In {
		down.Out <- in

	}
}
